create table registration_logs (
PlayerID int identity (0,1),
email Nvarchar(40) not null,
Country Nvarchar(30) not null,
BirthDate Date not null,
CreationDate Date not null,
CreationTime time not null,
DeletionDate Date null,
DeletionTime time null,
primary key (PlayerID)
)

create table players (
PlayerID int identity(0,1) not null,
Login Nvarchar(20) not null,
Password Nvarchar(20) not null,
Constraint FK_players Foreign Key (PlayerID) references registration_logs(PlayerID),
primary Key (PlayerID)
)

create table login_logs (
PlayerID int not null,
DateLogIn Date not null,
TimeLogIn time  not null,
DateLogOut Date null,
TimeLogOut time null,
primary key (PlayerID,Datelogin,TimeLogin)
)

/*
Je�li gracz ma DateLogOut null oraz TimeLogOut null,
to jest on aktualnie zalogowany na swoje konto w grze.
*/

create table premiums (
PremiumID int  identity (1,1) not null,
BonusGold float not null,
BonusExp float not null
primary key (PremiumID)
)

create table player_premium (
PlayerID int not null,
PremiumID int not null,
DateFrom Date not null,
DateTo Date not null
Constraint FK_player_premium1 Foreign Key (PlayerID) references Players(PlayerID)
ON DELETE CASCADE,
Constraint FK_player_premium2 Foreign Key (PremiumID) references premiums(PremiumID),
primary key (PlayerID,PremiumID)
)



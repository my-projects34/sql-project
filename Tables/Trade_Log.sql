/*
	Created: 31 January 2019

	Contains:
	TradeLog table to store all performed trades, also the unsuccessful ones.
*/

IF OBJECT_ID('TradeLog') IS NOT NULL
	DROP TABLE TradeLog

CREATE TABLE TradeLog(
	ID INT PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	CharName VARCHAR(32) NOT NULL,
	ItemName VARCHAR(32) NOT NULL,
	Amount INT NOT NULL,
	CharName2 VARCHAR(32) NOT NULL,
	ItemName2 VARCHAR(32) NOT NULL,
	Amount2 INT NOT NULL,

	Success BIT NOT NULL
)
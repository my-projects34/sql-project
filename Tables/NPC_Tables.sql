--you need to run character tables first

if object_ID ('NPC Refill') is not null
drop table [NPC Refill]

create table [NPC Refill] (
	CharacterId  int,
	ItemName varchar(32) not null,
	Quanity int not null,

	Constraint [FK_NPC Refill] Foreign Key (CharacterID) references characters on delete cascade
)
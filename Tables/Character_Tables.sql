--you need to run player and item tables first

if OBJECT_ID('[character outfit]') is not null
drop table [character outfit]

if OBJECT_ID('[character eq]') is not null
drop table [character eq]

if OBJECT_ID('[character stats]') is not null
drop table [character stats]

if OBJECT_ID('[character attributes]') is not null
drop table [character attributes]

if OBJECT_ID('[classes]') is not null
drop table [classes]

if OBJECT_ID('[character logs]') is not null
drop table [character logs]

if object_ID ('characters') is not null
drop table characters

create table characters (
	CharacterId  int not null identity,
	PlayerID int not null,
	CharacterName nvarchar(256) not null,
	Class nvarchar(32) not null,
	Race nvarchar(32) not null,
	Gender bit not null,
	[Level] int not null,
	Xp int not null,

	primary key (CharacterId),
	Constraint FK_characters Foreign Key (PlayerID) references players on delete cascade
)


create table [character attributes] (
	CharacterID int not null,
	Strength int not null,
	Dexternity int not null,
	Constitution int not null,
	Intelligence int not null,
	Wisdom int not null,
	Charisma int not null,

	primary key (CharacterID),
	Constraint [FK_character attributes] Foreign Key (CharacterID) references characters on delete cascade
)


create table [character stats] (
	CharacterID int not null,
	THAC0 int not null,
	BaseTHAC0 int not null,
	HP int not null,
	BaseHp int not null,
	ArmorClass int not null,
	BaseArmorClass int not null,
	Mana int not null,
	BaseMana int not null,
	EQSlots int not null,

	primary key (CharacterID),
	Constraint [FK_character stats] Foreign Key (CharacterID) references characters on delete cascade
)



create table [character eq] (
	CharacterID int not null,
	ItemName varchar(32) not null,
	Quanity int not null,

	Constraint [FK_character eq] Foreign Key (CharacterID) references characters on delete cascade
)



create table [character outfit] (
	CharacterID int not null,
	Head varchar(32),
	Body varchar(32),
	LeftArm varchar(32),
	RightArm varchar(32),
	Weapon varchar(32),
	Shield varchar(32),

	primary key (CharacterID),
	Constraint [FK_character outfit] Foreign Key (CharacterID) references characters on delete cascade
)

create table [character logs] (
	CharacterID int not null,
	[Date] time not null,
	[Description] varchar(4000) not null,

	primary key (CharacterID, [Date]),
	Constraint [FK_character logs] Foreign Key (CharacterID) references characters on delete cascade
)


create table classes (
	ClassName nvarchar(256),
	HPPerLvl int not null,
	ManaPerLvl int not null,
	THAC0PerLvl int not null,
	ArmorClassPerLvl int not null,

	Primary key (ClassName),
)

insert into classes  values
('Fighter', 10, 0, -2, -2),
('Ranger', 8, 1, -3, -1),
('Paladin', 9, 3, -1, -3),
('Cleric', 6, 7, -1, -1),
('Druid', 8, 5, -1, -1),
('Mage', 4, 10, -1, -1),
('Thief', 6, 0, -3, -1),
('Bard', 7, 4, -2, -1)
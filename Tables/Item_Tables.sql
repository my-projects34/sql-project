/*
	Created: 30 January 2019

	IMPORTANT: First initialize Check_Item_Tables PROCEDURE

	Contains: 
		Main table for all items and subtables for subcategories if needed (e.g. swords have a subtable containing their attributes like damage etc.)
		The completeness of all the data is being checked at the end of the generation by the execution of Check_Item_Tables procedure.
		It is used to specify whether all items in subtables have their general references in the main table.
*/

SET NOCOUNT ON

IF OBJECT_ID('Amulet') IS NOT NULL
	DROP TABLE Amulet

IF OBJECT_ID('Food') IS NOT NULL
	DROP TABLE Food

IF OBJECT_ID('Spell') IS NOT NULL
	DROP TABLE Spell

IF OBJECT_ID('Potion') IS NOT NULL
	DROP TABLE Potion

IF OBJECT_ID('Armor') IS NOT NULL
	DROP TABLE Armor

IF OBJECT_ID('Shield') IS NOT NULL
	DROP TABLE Shield

IF OBJECT_ID('Wand') IS NOT NULL
	DROP TABLE Wand

IF OBJECT_ID('Sword') IS NOT NULL
	DROP TABLE Sword

IF OBJECT_ID('Loot_Trigger') IS NOT NULL
	DROP TRIGGER Loot_Trigger

IF OBJECT_ID('Loot') IS NOT NULL
	DROP TABLE Loot

IF OBJECT_ID('Loot_Boxes') IS NOT NULL
	DROP TABLE Loot_Boxes

IF OBJECT_ID('Items') IS NOT NULL
	DROP TABLE Items

IF OBJECT_ID('Categories') IS NOT NULL
	DROP TABLE Categories

IF OBJECT_ID('Tiers') IS NOT NULL
	DROP TABLE Tiers

CREATE TABLE Categories(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL
)

INSERT INTO Categories([Name]) VALUES
('Currency'),
('Loot Box'),
('Sword'),
('Wand'),
('Shield'),
('Armor'),
('Potion'),
('Spell'),
('Food'),
('Amulet'),
('Miscellaneous')


CREATE TABLE Tiers(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL
)

INSERT INTO Tiers([Name]) VALUES
('Common'),
('Uncommon'),
('Rare'),
('Epic'),
('Legendary')

CREATE TABLE Items(
	ID INT IDENTITY(1, 1) NOT NULL,
	Category VARCHAR(32) NOT NULL,
	Tier VARCHAR(32) NOT NULL,
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	MaxStack INT NOT NULL,

	CONSTRAINT FK_Cat FOREIGN KEY (Category)
    REFERENCES Categories([Name]),

	CONSTRAINT FK_Tier FOREIGN KEY (Tier)
    REFERENCES Tiers([Name])
)

INSERT INTO Items VALUES
	-- Currency
	('Currency', 'Common', 'Gold Coin', 100),

	-- Loot Boxes
	('Loot Box', 'Legendary', 'Undermoon Box', 9999),

	-- Swords
	('Sword', 'Common', 'Steel Sword', 1),
	('Sword', 'Common', 'Bronze Sword', 1),
	('Sword', 'Common', 'Bone Sword', 1),
	('Sword', 'Common', 'Broadsword', 1),
	('Sword', 'Common', 'Dagger', 1),
	('Sword', 'Uncommon', 'Blacksteel Sword', 1),
	('Sword', 'Uncommon', 'Poison Dagger', 1),
	('Sword', 'Rare', 'Crystal Sword', 1),
	('Sword', 'Rare', 'Ice Rapier', 1),
	('Sword', 'Epic', 'Fire Sword', 1),
	('Sword', 'Epic', 'Magic Longsword', 1),
	('Sword', 'Legendary', 'Excalibur', 1),

	-- Wands
	('Wand', 'Rare', 'Dream Blossom Staff', 1),
	('Wand', 'Epic', 'Wand of Darkness', 1),
	('Wand', 'Epic', 'Wand of Inferno', 1),
	('Wand', 'Legendary', 'Wand of Starstorm', 1),

	-- Shields
	('Shield', 'Common', 'Wooden Shield', 1),
	('Shield', 'Uncommon', 'Battle Shield', 1),
	('Shield', 'Rare', 'Ancient Shield', 1),
	('Shield', 'Epic', 'Blessed Shield', 1),
	('Shield', 'Legendary', 'Dragon Shield', 1),

	-- Armors
	('Armor', 'Common', 'Coat', 1),
	('Armor', 'Uncommon', 'Chain Armor', 1),
	('Armor', 'Rare', 'Dark Armor', 1),
	('Armor', 'Epic', 'Crown Armor', 1),
	('Armor', 'Legendary', 'Dragon Robe', 1),
	
	-- Potions
	('Potion', 'Common', 'Water', 1),
	('Potion', 'Uncommon', 'Antidote', 1),
	('Potion', 'Uncommon', 'Poison', 1),
	('Potion', 'Rare', 'Mana Potion', 1),
	('Potion', 'Rare', 'Health Potion', 1),
	('Potion', 'Epic', 'Darkforest Water', 1),
	('Potion', 'Legendary', 'Dragonbreath Potion', 1),

	-- Spells
	('Spell', 'Uncommon', 'Fireball', 100),
	('Spell', 'Uncommon', 'Curse', 100),
	('Spell', 'Rare', 'Ice Strike', 100),
	('Spell', 'Epic', 'Fire Wave', 100),
	('Spell', 'Epic', 'Eternal Winter', 100),
	('Spell', 'Legendary', 'Divine Missle', 100),
	('Spell', 'Legendary', 'Ultimate Flame Strike', 100),

	-- Food
	('Food', 'Common', 'Bread', 100),
	('Food', 'Common', 'Candy', 100),
	('Food', 'Common', 'Meat', 100),
	('Food', 'Uncommon', 'Grapes', 100),
	('Food', 'Rare', 'Fire Mushroom', 100),
	('Food', 'Epic', 'Forbidden Fruit', 100),

	-- Amulets
	('Amulet', 'Uncommon', 'Bronze Amulet', 1),
	('Amulet', 'Rare', 'Crystal Necklace', 1),
	('Amulet', 'Rare', 'Dragon Necklace', 1),
	('Amulet', 'Epic', 'Ancient Amulet', 1),
	('Amulet', 'Epic', 'Amulet of Loss', 1),
	('Amulet', 'Legendary', 'Elven Amulet', 1),

	-- Miscellaneous
	('Miscellaneous', 'Common', 'Dice', 1),
	('Miscellaneous', 'Common', 'Hammer', 1),
	('Miscellaneous', 'Common', 'Leather', 10),
	('Miscellaneous', 'Common', 'Empty Flask', 1),
	('Miscellaneous', 'Common', 'Key', 1),
	('Miscellaneous', 'Common', 'Steel', 10),
	('Miscellaneous', 'Common', 'Wood', 10),
	('Miscellaneous', 'Uncommon', 'Silver Goblet', 1),
	('Miscellaneous', 'Rare', 'Crystal', 100),
	('Miscellaneous', 'Rare', 'Black Pearl', 100),
	('Miscellaneous', 'Epic', 'Phoenix Ash', 100),
	('Miscellaneous', 'Epic', 'Diamond', 100),
	('Miscellaneous', 'Legendary', 'White Pearl', 100)


-- SUBTABLES FOR ITEMS
CREATE TABLE Amulet(
	[Name] VARCHAR(32) PRIMARY KEY,
	Strength INT NOT NULL,
	Dexternity INT NOT NULL,
	Constitution INT NOT NULL,
	Intelligence INT NOT NULL,
	Wisdom INT NOT NULL,
	Charisma INT NOT NULL,

	Health INT NOT NULL,
	Mana INT NOT NULL,
)

INSERT INTO Amulet VALUES
('Bronze Amulet', 0, 0, 0, 0, 0, 5, 50, 0),
('Crystal Necklace', 10, 5, 5, 0, 0, 15, 80, 100),
('Dragon Necklace', 15, 15, 0, 0, 5, 15, 200, 50),
('Ancient Amulet', 20, 15, 5, 5, 10, 20, 0, 300),
('Amulet of Loss', 0, 0, 0, 0, 0, 0, 0, 0), -- makes you not lose items when you die
('Elven Amulet', 30, 30, 30, 30, 30, 30, 300, 1000)


CREATE TABLE Food(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	HP INT NOT NULL,
	Mana INT NOT NULL
)

INSERT INTO Food VALUES
('Bread', 10, 0),
('Candy', 7, 0),
('Meat', 18, 0),
('Grapes', 4, 0),
('Fire Mushroom', 120, 60),
('Forbidden Fruit', 1000, 1000)



CREATE TABLE Spell(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	Damage INT NOT NULL,
	CriticalDamage INT NOT NULL,
	[Range] INT NOT NULL,
	Mana INT NOT NULL
)

INSERT INTO Spell VALUES
('Fireball', 30, 50, 8, 10),
('Curse', 25, 35, 15, 13),
('Ice Strike', 10, 20, 25, 8),
('Fire Wave', 35, 60, 17, 50),
('Eternal Winter', 70, 100, 20, 90),
('Divine Missle', 150, 200, 30, 250),
('Ultimate Flame Strike', 350, 450, 35, 550)

CREATE TABLE Armor(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	Defence INT NOT NULL
)

INSERT INTO Armor VALUES
('Coat', 1),
('Chain Armor', 10),
('Dark Armor', 15),
('Crown Armor', 30),
('Dragon Robe', 50)


CREATE TABLE Shield(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	Defence INT NOT NULL
)

INSERT INTO Shield VALUES
('Wooden Shield', 5),
('Battle Shield', 8),
('Ancient Shield', 13),
('Blessed Shield', 25),
('Dragon Shield', 40)


CREATE TABLE Wand(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	Damage INT NOT NULL,
	CriticalDamage INT NOT NULL,
	[Range] INT NOT NULL,
	Mana INT NOT NULL
)

INSERT INTO Wand VALUES
('Dream Blossom Staff', 70, 100, 10, 30),
('Wand of Darkness', 130, 150, 12, 40),
('Wand of Inferno', 145, 170, 13, 60),
('Wand of Starstorm', 180, 220, 15, 130)

CREATE TABLE Sword(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	Damage INT NOT NULL,
	CriticalDamage INT NOT NULL,
	MagicDamage INT NOT NULL
)

INSERT INTO Sword VALUES
('Steel Sword', 10, 15, 0),
('Bronze Sword', 7, 10, 0),
('Bone Sword', 2, 5, 1),
('Broadsword', 15, 20, 0),
('Dagger', 5, 10, 0),
('Blacksteel Sword', 20, 25, 0),
('Poison Dagger', 30, 45, 0),
('Crystal Sword', 40, 65, 10),
('Ice Rapier', 30, 70, 10),
('Fire Sword', 60, 100, 30),
('Magic longsword', 65, 130, 50),
('Excalibur', 100, 200, 75)

CREATE TABLE Loot(
	Loot_Box_Name VARCHAR(32) NOT NULL,
	Item_Name VARCHAR(32) NOT NULL,
	Chance INT NOT NULL -- 0 TO 100

	PRIMARY KEY(Loot_Box_Name, Item_Name)
)

GO
CREATE TRIGGER Loot_Trigger ON Loot
INSTEAD OF INSERT
AS
BEGIN TRY
	DECLARE @LootBoxName VARCHAR(32)
	DECLARE @Item_Name VARCHAR(32)
	DECLARE @Chance INT

	DECLARE Cur CURSOR FOR SELECT * FROM inserted
	OPEN Cur
	
	FETCH Cur INTO @LootBoxName, @Item_Name, @Chance

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF EXISTS(SELECT * FROM Items WHERE [Name] = @LootBoxName)
		BEGIN
			IF EXISTS(SELECT * FROM Items WHERE [Name] = @Item_Name) AND @Chance BETWEEN 0 AND 100
			BEGIN
				INSERT INTO Loot VALUES
				(@LootBoxName, @Item_Name, @Chance)
			END ELSE
			BEGIN
				RAISERROR('Cannot add item %s to LootBox %s', 16, 1, @Item_Name, @LootBoxName)
			END
		END ELSE
		BEGIN
			RAISERROR('%s is not a loot box or does not exist', 16, 1, @LootBoxName)
		END
		FETCH Cur INTO @LootBoxName, @Item_Name, @Chance
	END


	CLOSE Cur
	DEALLOCATE Cur
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(4000)
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	SET @ErrorMessage = ERROR_MESSAGE()
	SET @ErrorSeverity = ERROR_SEVERITY()
	SET @ErrorState = ERROR_STATE()

	EXEC ErrorDisplayer @ErrorMessage, @ErrorSeverity, @ErrorState
END CATCH
GO

INSERT INTO Loot VALUES
('Undermoon Box', 'Gold Coin', 50),
('Undermoon Box', 'Wand of Inferno', 70)

--SELECT * FROM Loot


-- FINAL DATA CHECK
DECLARE @Status BIT
EXEC Check_Item_Tables @Return_Status = @Status OUTPUT -- procedure that checks data completeness 
--you need to run character tables first

If OBJECT_ID('LevelTr') is not null
	drop trigger LevelTr
go

create trigger LevelTr on Characters
After update
as	
		if(update(xp))
		begin
			declare i cursor
			for select CharacterID, xp from characters
			for update of [level], XP
			declare @characterID int, @XP int

			open i
			fetch i into  @characterID, @XP
		
			while @@FETCH_STATUS<>-1
			begin 
			
				while(@XP >=1000)
				begin
					exec LevelUp @characterID
					set @XP = @XP - 1000
					update characters set Xp = @XP, [Level] = [Level] + 1 where current of i
				end
				fetch i into  @characterID, @XP
			end
			close i
			deallocate i
		end

go
--you need to run character utilities first

if OBJECT_ID('AddNPC') is not null
	drop proc AddNPC
go

create proc AddNPC (@NPCName nvarchar(256),  @class nvarchar(256),
			@race nvarchar(256), @gender bit, @strength int, @dexternity int, @constitution int,
			@intelligence int, @wisdom int, @charisma int )
as
	declare @rootID int
	set @rootID = 0
	exec AddCharacter @rootID, @NPCName, @class, @race, @gender, @strength, @dexternity, @constitution,
					@intelligence, @wisdom, @charisma
	update [character stats] set EQslots = 10000000 where characterID = (select CharacterId from characters where
				PlayerID = @rootID and CharacterName = @NPCName)
go

if OBJECT_ID('Refill') is not null
	drop proc Refill
go

create proc Refill(@NPCName nvarchar(256))
as
	declare @rootID int
	set @rootID = 0

	declare @NPCID int
	select @NPCID = CharacterId from characters where
				PlayerID = @rootID and CharacterName = @NPCName
	delete [character eq] where CharacterID = @NPCID

	declare i_ cursor
	for select CharacterID, ItemName, Quanity from [NPC Refill]
	for read only
	declare @tempCharacterID nvarchar(256), @tempItemName varchar(256)
	declare @tempQuanity int
	declare @garbage bit
	open i_
		fetch i_ into  @tempCharacterID, @tempItemName, @tempQuanity
		while @@FETCH_STATUS<>-1
		begin 
			if(@tempCharacterID = @NPCID)
			begin
				exec GiveItem @NPCID, @tempItemName, @tempQuanity, @garbage
			end

			fetch i_ into  @tempCharacterID, @tempItemName, @tempQuanity
		end
		close i_
		deallocate i_

go

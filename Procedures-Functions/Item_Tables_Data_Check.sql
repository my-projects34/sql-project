/*
	Created: 30 January 2019

	- Check_Item_Tables PROCEDURE Checks whether all data in Item Tables is correct and items have their references in subtables if needed.
	- CategoriesToCheck TABLE is used as the index of categories to check.
*/

IF OBJECT_ID('CategoriesToCheck') IS NOT NULL
	DROP TABLE CategoriesToCheck

IF OBJECT_ID('Check_Item_Tables') IS NOT NULL
	DROP PROCEDURE Check_Item_Tables


CREATE TABLE CategoriesToCheck
(
	Category_Name VARCHAR(32) NOT NULL
)

INSERT INTO CategoriesToCheck VALUES
('Sword'),
('Wand'),
('Shield'),
('Armor'),
('Spell'),
('Food'),
('Amulet')


GO
CREATE PROCEDURE Check_Item_Tables(@Return_Status BIT OUTPUT)
AS
BEGIN
	SET @Return_Status = 1
	DECLARE @Name1 VARCHAR(32)
	DECLARE @Name2 VARCHAR(32)
	DECLARE @tempName VARCHAR(32)

	DECLARE @CAT_NAME VARCHAR(32)
	DECLARE GLOBAL_CURSOR CURSOR FOR SELECT * FROM CategoriesToCheck
	OPEN GLOBAL_CURSOR
	FETCH GLOBAL_CURSOR INTO @CAT_NAME
	WHILE @@FETCH_STATUS = 0
	BEGIN
		CREATE TABLE TempTable(
			Name1 VARCHAR(32),
			Name2 VARCHAR(32)
		)

		DECLARE @Querry NVARCHAR(512)
		SET @Querry = 
			'
			INSERT INTO TempTable
			SELECT I.Name, S.Name FROM Items AS I FULL JOIN ' + CONVERT(NVARCHAR(32), @CAT_NAME) + ' as S
			ON I.Name = S.Name
			WHERE Category = ''' + CONVERT(NVARCHAR(32), @CAT_NAME) + ''' and S.Name IS NULL OR I.Name IS NULL
			'
		
		EXEC sp_executesql @Querry
		
		DECLARE Cur CURSOR FOR SELECT * FROM TempTable
		OPEN Cur
		
		FETCH Cur INTO @Name1, @Name2

		WHILE @@FETCH_STATUS = 0 
		BEGIN
			IF (@Name1 IS NULL)
				SET @Return_Status = 0
				PRINT(@Name2 + ' is missing in the items table.')
			IF (@Name2 IS NULL)
				SET @Return_Status = 0
				PRINT(@Name1 + ' is missing in the ' + CONVERT(NVARCHAR(32), @CAT_NAME) + ' subtable.')
			FETCH Cur INTO @Name1, @Name2
		END

		CLOSE Cur
		DEALLOCATE Cur

		DROP TABLE TempTable
		FETCH GLOBAL_CURSOR INTO @CAT_NAME
	END

	CLOSE GLOBAL_CURSOR
	DEALLOCATE GLOBAL_CURSOR

	

	
	IF @Return_Status = 1
	BEGIN
		PRINT('Item tables are complete.')
	END
END
GO
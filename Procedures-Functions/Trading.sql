/*
	Created: 31 January 2019

	Contains 
	1) 'TradeRequest' procedure

	Desc:
	1) Performs a trade request using takeItem and giveItem procedures, then stores the information about a trade in trade logs table.
*/

IF OBJECT_ID('TradeRequest') IS NOT NULL
	DROP PROCEDURE TradeRequest

GO
CREATE PROCEDURE TradeRequest(@charID INT, @char2ID INT, @item1 VARCHAR(32), @amount1 INT, @item2 VARCHAR(32), @amount2 INT, @result BIT OUTPUT)
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
		DECLARE @result1 BIT = 0

		SET @result = 0

		DECLARE @player1ID INT
		DECLARE @player2ID INT

		SET @player1ID = (SELECT PlayerID FROM characters WHERE CharacterId = @charID)
		SET @player2ID = (SELECT PlayerID FROM characters WHERE CharacterId = @char2ID)

		IF (@player1ID IS NOT NULL) AND (@player2ID IS NOT NULL)
		BEGIN
			DECLARE @firstLogged BIT
			DECLARE @secondLogged BIT

			SET @firstLogged = dbo.is_logged2(@player1ID)
			SET @secondLogged = dbo.is_logged2(@player2ID)

			IF (@firstLogged = 1) AND (@secondLogged = 1)
			BEGIN
				EXEC takeItem @charID, @item1, @amount1, @result1 OUTPUT
				IF @result1 = 1
				BEGIN
					EXEC giveItem @charID, @item2, @amount2, @result1 OUTPUT
					IF (@result1 = 1)
					BEGIN
						EXEC takeItem @char2ID, @item2, @amount2, @result1 OUTPUT
						IF (@result1 = 1)
						BEGIN
							EXEC giveItem @char2ID, @item1, @amount1, @result1 OUTPUT
							IF (@result1 = 1)
							BEGIN
								SET @result = 1
								COMMIT
							END
						END
					END
				END	
			END		
		END

		IF @result = 0
		BEGIN
			ROLLBACK
			RAISERROR('Trade failed', 16, 1)
		END

		IF OBJECT_ID('TradeLog') IS NOT NULL
		BEGIN
			INSERT INTO TradeLog(CharName, ItemName, Amount, CharName2, ItemName2, Amount2, Success) VALUES
			(@charID, @item1, @amount1, @char2ID, @item2, @amount2, @result)
		END
	END TRY
	BEGIN CATCH
		SET @result = 0
		IF OBJECT_ID('TradeLog') IS NOT NULL
		BEGIN
			INSERT INTO TradeLog(CharName, ItemName, Amount, CharName2, ItemName2, Amount2, Success) VALUES
			(@charID, @item1, @amount1, @char2ID, @item2, @amount2, @result)
		END
	END CATCH
END
GO
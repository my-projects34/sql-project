--you need to run Character_Tables.sql first


if OBJECT_ID('AddCharacter') is not null
	drop proc AddCharacter
go

create proc AddCharacter(@playerID int, @characterName nvarchar(256),  @class nvarchar(256),
			@race nvarchar(256), @gender bit, @strength int, @dexternity int, @constitution int,
			@intelligence int, @wisdom int, @charisma int )
as
	begin try
		declare @error nvarchar(256) 
		if( not exists ( select *from players where players.PlayerID = @playerID))
		begin
			set @error = 'Player ' + cast (@playerID as nvarchar) + ' does not exists' 
			raiserror (@error, 16, 1)
		end

		if (exists ( select * from characters  as c where c.CharacterName = @characterName and c.PlayerID = @playerID) )
		begin
			set @error =  'Player ' + cast (@playerID as nvarchar) + ' already have character named ' + @characterName 
			raiserror (@error, 16, 1)
		end

		if (not exists (select * from classes where ClassName = @class ) )
		begin 
			set @error = 'Class ' + @class + ' does not exists'
			raiserror (@error, 16, 1)
		end

		insert into characters values
		(@playerID, @characterName, @class, @race, @gender, 1, 0)
		declare @characterID int
		select @characterID =CharacterID from characters as c where c.PlayerID = @playerID and c.CharacterName = @characterName
		
		insert into [character attributes] values
		(@characterID, @strength, @dexternity, @constitution, @intelligence, @wisdom, @charisma)

		insert into [character stats] values
		(@characterID, 20, 20, 10, 10, 9, 9, 10, 10, 20)

		insert into [character logs] values
		(@characterID, GETDATE(), 'New character is made')
	end try

	begin catch
		exec GetErrorInfo
	end catch
go

if OBJECT_ID('GiveItem') is not null
	drop proc GiveItem
go

create proc GiveItem(@characterID int, @itemName varchar(32), @quanity int, @result bit OUT)
as
	begin transaction
	begin try
		declare @error nvarchar(256) 
		if( not exists ( select *from characters where characters.CharacterId = @characterID))
		begin
			set @error = 'Character ' + cast (@characterID as nvarchar) + ' does not exists' 
			raiserror (@error, 16, 1)
		end

		if( not exists ( select *from items where Items.Name = @itemName))
		begin
			set @error = 'Item ' + @itemName + ' does not exists' 
			raiserror (@error, 16, 1)
		end

		declare i cursor
		for select CharacterID, ItemName, Quanity from [character eq]
		for update of Quanity
		declare @tempCharacterID nvarchar(256), @tempItemName varchar(256)
		declare @tempQuanity int
		declare @itemStackMax int
		declare @maxSlots int
		declare @currentSlots int
		set @currentSlots = 0
		select @itemStackMax = items.MaxStack from items where Items.Name = @itemName
		select @maxSlots =[character stats].EQSlots from [character stats]

		open i
		fetch i into  @tempCharacterID, @tempItemName, @tempQuanity
		while (@@FETCH_STATUS<>-1 and @quanity>0)
		begin 
			if(@tempCharacterID = @characterID)
			begin
				set @currentSlots = @currentSlots +1 --duh
				if(@tempItemName = @itemName)
				begin
					if(@tempQuanity < @itemStackMax)
					begin 
						declare @tempStack int
						set @tempStack = @itemStackMax - @tempQuanity
						set @quanity = @quanity - @tempStack
						if(@quanity >= 0)
						begin
							update [character eq] set Quanity = Quanity + @tempStack where current of i
						end
						else
						begin
							set @quanity = @quanity + @tempStack
							update [character eq] set Quanity = Quanity + @quanity where current of i
							set @quanity = 0
						end
					end
				end
			end


			if(@currentSlots = @maxSlots)
			begin
				
				raiserror('not enough place in eq', 16, 1)
			end

			fetch i into  @tempCharacterID, @tempItemName, @tempQuanity
		end
		close i
		deallocate i

		if(@quanity > 0)
		--that means, that we didnt put everything on the stacks we already had
		begin
			while (@quanity >0)
			begin
			if(@currentSlots = @maxSlots)
				begin
					raiserror('not enough place in eq', 16, 1)
				end

				if(@quanity > @itemStackMax)
				begin
					set @quanity = @quanity - @itemStackMax
					insert into [character eq] values
					(@characterID, @itemName, @itemStackMax)
				end
				else
				begin
					insert into [character eq] values
					(@characterID, @itemName, @quanity)
					set @quanity = 0
				end
			end
		end

		set @result = 1
		if @@trancount > 0 commit transaction
	end try
	begin catch
		set @result = 0 
		exec GetErrorInfo
		if @@trancount > 0 rollback transaction
	end catch
go


if OBJECT_ID('TakeItem') is not null
	drop proc TakeItem
go

create proc TakeItem(@characterID int, @itemName varchar(32), @quanity int, @result bit OUT)
as
	begin transaction
	begin try
		declare @error nvarchar(256) 
		if( not exists ( select *from characters where characters.CharacterId = @characterID))
		begin
			set @error = 'Character ' + cast (@characterID as nvarchar) + ' does not exists' 
			raiserror (@error, 16, 1)
		end

		if( not exists ( select *from items where Items.Name = @itemName))
		begin
			set @error = 'Item ' + @itemName + ' does not exists' 
			raiserror (@error, 16, 1)
		end


		declare i cursor
		for select CharacterID, ItemName, Quanity from [character eq]
		for update of Quanity
		declare @tempCharacterID nvarchar(256), @tempItemName varchar(256)
		declare @tempQuanity int

		open i
		fetch i into  @tempCharacterID, @tempItemName, @tempQuanity
		while (@@FETCH_STATUS<>-1 and @quanity >0)
		begin 
			if(@tempCharacterID = @characterID)
			begin
				if(@tempItemName = @itemName)
				begin
					if(@tempQuanity>@quanity)
					begin
						update [character eq] set Quanity = Quanity - @quanity where current of i
						set @quanity = 0
					end
					else
					begin
						set @quanity = @quanity - @tempQuanity
						delete from [character eq] where current of i
					end
				end
			end
			fetch i into  @tempCharacterID, @tempItemName, @tempQuanity
		end
		close i
		deallocate i
		if(@quanity > 0)
		begin
			set @error = 'Character doesn''t have enough of item ' + @itemName
			raiserror(@error, 16, 1)
		end

		set @result = 1
		if @@trancount > 0 commit transaction
	end try
	begin catch
		set @result = 0 
		exec GetErrorInfo
		if @@trancount > 0 rollback transaction
	end catch
go

if OBJECT_ID('Reload') is not null
	drop proc Reload
go

create proc Reload(@characterID int)
as
	update [character stats] set ArmorClass = BaseArmorClass, HP = BaseHp, Mana = BaseMana,
	THAC0 = BaseTHAC0 where CharacterID = @characterID
--here we reload character stats
--basically we make for example ArmorClass = BaseArmorClass
--and the get the modifiers from armour and stuff
--same with everything else in stats
go


if OBJECT_ID('LevelUp') is not null
	drop proc LevelUp
go

create proc LevelUp(@characterID int)
as

	declare @class nvarchar(256)
	select @class = class from characters where CharacterId = @characterID
	declare @HPPerLvl int, @ManaPerLvl int
	declare @THAC0PerLvl int, @ArmorClassPerLvl int
	select @HPPerLvl= HPPerLvl, @ManaPerLvl = ManaPerLvl , 
			@THAC0PerLvl =THAC0PerLvl, @ArmorClassPerLvl = ArmorClassPerLvl 
			from classes where ClassName = @class
	update [character stats] set BaseArmorClass = BaseArmorClass + @ArmorClassPerLvl,
			BaseHp = BaseHp + @HPPerLvl, BaseMana = BaseMana + @ManaPerLvl, BaseTHAC0 = BaseTHAC0 + @THAC0PerLvl
	where CharacterId = @characterID
	Exec Reload @characterID


	declare @message nvarchar(256)
	declare @lvl int
	select @lvl = [level] from characters where CharacterId = @characterID
	set @message = 'level up ( from ' + cast(@lvl as nvarchar) + ' to ' + cast(@lvl+1 as nvarchar) + ' )' 
	insert into [character logs] values
	(@characterID, GETDATE(), @message)
go


if OBJECT_ID('GetGold') is not null
	drop proc GetGold
go

create proc GetGold(@money int, @characterID int)
as
	declare @trash bit
	declare @playerID int
	select @playerID = PlayerID from characters where characterID = @characterID
	if exists (Select * from player_premium where PlayerID = @playerID)
	begin
		declare @premium_id int = (Select PremiumID from player_premium where PlayerID = @playerID)
		set @money = @money*(1+(Select SUM(BonusGold) from premiums where PremiumID = @premium_id))
	end 
	exec GiveItem @characterID, 'Gold Coin',@money, @trash
go


if OBJECT_ID('GetXP') is not null
	drop proc GetXP
go

create proc GetXP(@exp int, @characterID int)
as
	declare @trash bit
	declare @playerID int
	select @playerID = PlayerID from characters where characterID = @characterID
	if exists (Select * from player_premium where PlayerID = @playerID)
	begin
		declare @premium_id int = (Select PremiumID from player_premium where PlayerID = @playerID)
		set @exp = @exp*(1+(Select SUM(BonusExp) from premiums where PremiumID = @premium_id))
	end 
	update characters set Xp = @exp where CharacterId = @characterID
go


--that's only for creating for accounts
Exec create_account 'root@root.root', 'rootroot', 'rootroot', 'rootroot', 'root', '2000-03-25'
Exec create_account 'rafik272@interia.pl','rafik272','heheloler','heheloler','Poland','2000-02-15'
Exec create_account 'rafik273@interia.pl','rafik273','heheloler','heheloler','Poland','2000-02-15'
Exec create_account 'rafik274@interia.pl','rafik274','heheloler','heheloler','Poland','2000-02-15'
Exec create_account 'rafik275@interia.pl','rafik275','heheloler','heheloler','Poland','2000-02-15'


Exec create_account 'admin@aureo.com','Kacprate','123456789','123456789','World','1970-01-01'
exec AddCharacter 1, 'Kacprate', 'Sorcerer', 'Human', 1, 17, 13, 16, 12, 13, 17

exec AddCharacter 1, 'Ajanatis', 'Paladin', 'Human', 1, 17, 13, 16, 12, 13, 17
exec AddCharacter 1, 'Alora', 'Thief','Halfling', 0, 8, 19, 12, 14, 7, 10
exec AddCharacter 1, 'Branwen', 'Cleric','Human', 0, 13, 16, 15, 9, 16, 13
exec AddCharacter 2, 'Coran', 'Thief','Elf', 1, 14, 20, 12, 14, 9, 16 
exec AddCharacter 2, 'Dynaheir', 'Mage','Human', 0, 11, 13, 16, 17, 15, 12 
exec AddCharacter 3, 'Edwin', 'Mage','Human', 1, 9, 10, 16, 18, 9, 10
exec AddCharacter 4, 'Eldoth', 'Bard','Human', 1, 16, 12, 15, 13, 10, 16
exec AddCharacter 4, 'Faldorn', 'Druid','Human', 0, 12, 15, 11, 10, 16, 15
exec AddCharacter 4, 'Garrick', 'Bard','Human', 1, 14, 16, 9, 13, 14, 15    
exec AddCharacter 4, 'Imoen', 'Thief','Human', 0, 9, 18, 16, 17, 11, 16 
exec AddCharacter 4, 'Jaheira', 'Druid','Human', 0, 15, 14, 17, 10, 14, 15 
exec AddCharacter 4, 'Kagain', 'Fighter','Dwafr', 1, 16, 12, 20, 15, 11, 8 
exec AddCharacter 4, 'Khalid', 'Fighter','Half-Elf', 1, 15, 16, 17, 12, 10, 9 
exec AddCharacter 4, 'Kivan', 'Ranger','Elf', 1, 18, 17, 14, 10, 14, 8 
exec AddCharacter 4, 'Minsc', 'Ranger','Human', 1, 18, 15, 15, 8, 6, 9 
exec AddCharacter 4, 'Montaron', 'Thief','Halfing', 1, 16, 17, 15, 12, 13, 9 
exec AddCharacter 4, 'Quayle', 'Mage','Gnome', 1, 8, 15, 11, 17, 10, 6
exec AddCharacter 4, 'Safana', 'Thief','Human', 0, 13, 17, 12, 16, 9, 17 
exec AddCharacter 4, 'Shar-Teel', 'Fighter','Human', 0, 18, 17, 9, 14, 7, 11
exec AddCharacter 4, 'Skie', 'Thief','Human', 0, 11, 18, 15, 15, 8, 13
exec AddCharacter 4, 'Tiax', 'Clerid','Gnome', 1, 9, 16, 16, 10, 12, 9  
exec AddCharacter 4, 'Viconia', 'Cleric','Drow', 0, 10, 19, 8, 16, 15, 14 
exec AddCharacter 4, 'Xan', 'Mage','Elf', 1, 13, 16, 7, 17, 14, 16 
exec AddCharacter 4, 'Xzar', 'Mage','Human', 1, 14, 16, 10, 17, 16, 10 
exec AddCharacter 4, 'Yeslick', 'Fighter','Dwarf', 1, 15, 13, 17, 7, 16, 10 

/*select * from characters
select * from [character stats]
select * from [character outfit]
select * from [character logs]
select * from [character attributes]
select * from [character eq] */
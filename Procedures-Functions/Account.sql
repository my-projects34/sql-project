If object_Id ('create_account') is not null
drop proc create_account
go
Create proc create_account (@email Nvarchar(40), @login Nvarchar(30), @password Nvarchar(30), @password2 Nvarchar(30), @country Nvarchar(30), @BirthDate Date) -- tworzenie konta
as 
begin try
IF @email is null or @login is null or @password is null or @password2 is null or @country is null or @BirthDate is null -- je�li jaki� parametr nie zosta� podany
begin
	Raiserror ('Fill in the empty field',16,1)
end
IF ((Select CHARINDEX('@',@email)) = 0 or (Select CHARINDEX('.',@email)) = 0) -- je�li w emailu brakuje "." lub "@"
begin
	Raiserror ('Wrong email',16,1)
end
IF exists (Select * from registration_logs as R join players as P on R.PlayerID = P.PlayerID where email = @email) -- je�li ten email ju� jest przypisany do jakiego� konta
begin
	Raiserror ('This email adress is already in the database',16,1)
end
IF (Len(@login)<8 or Len(@login)>20) -- je�li login nie mie�ci si� w zakresie d�ugo�ci [8;20]
begin
	Raiserror ('Login''s length is not correct',16,1)
end
IF exists (Select * from players where Login = @login) -- je�li login jest ju� zaj�ty
begin
	Raiserror('This login has been already taken',16,1)
end
IF (Len(@password)<8 or Len(@password)>20) -- je�li has�o nie mie�ci si� w zakresie [8;20]
begin
	Raiserror ('Password''s length is not correct',16,1)
end
IF @password <> @password2 -- je�li powt�rzenie has�a jest inne ni� pierwsze podane has�o
begin
	Raiserror('Second password entered incorrectly',16,1)
end
IF (Datediff(dd,@BirthDate,GETDATE())/365) < 18 -- je�li osoba nie ma uko�czonych 18 lat
begin
	Raiserror('You are under 18',16,1)
end

insert into registration_logs values (@email, @country, @BirthDate, GetDate(), SYSDATETIME(), null, null) -- dodajemy logi rejestracji odpowiedniego gracza
insert into players values (@login,@password) -- dodajemy osob� do tabeli players

Print 'Account ' + @login + ' created.'
end try

begin catch
	exec GetErrorInfo
end catch
Go

IF object_id ('Log_In') is not null
drop proc Log_In 
go
create procedure Log_In (@login Nvarchar(30), @password Nvarchar(30)) -- logowanie si� do gry
as
begin try
declare @ID int -- do wyszukiwania w logach_logowania
if exists (Select * from players where Login = @login and Password = @password) -- Je�li w bazie znajduje si� taki gracz
begin
	Set @ID = dbo.get_id(@login) -- u�ywamy pomocniczej funkcji
	IF dbo.is_logged(@login) = 0 -- Je�li gracz nie jest aktualnie zalogowany
	begin
		insert into login_logs values (@ID,GetDate(),SYSDATETIME(),null,null) -- logujemy gracza
		Print 'Player ' + @login + ' logs-in.'
	end
	else
	begin
		Raiserror ('You are currently logged-in',16,1)
	end
end
else
begin 
	Raiserror('Your account doesn''t exist. Check your login or password',16,1) -- je�li w bazie nie ma takiego gracza
end
end try
begin catch
	exec GetErrorInfo
end catch
go

If object_id('insert_login_logs') is not null -- trigger kt�ry odpowiada za kontolowanie dodawania log�w, maksymalnie przechowywanych jest 20 ostatnich logowa� ka�dego gracza.
drop trigger insert_login_logs
go
create trigger insert_login_logs
on login_logs instead of insert
as
declare @PlayerID int
declare @DateLogIn Date
declare @TimeLogIn time
declare @DateLogOut Date
declare @TimeLogOut time
declare kursor cursor for Select * from inserted -- kursor na tabeli inserted
for read only
open kursor
fetch kursor into @PlayerID, @DateLogIn, @TimeLogIn, @DateLogOut, @TimeLogOut -- pobieramy 1 wiersz
while @@FETCH_STATUS <> -1 -- dop�ki istniej� wiersze w tabeli inserted
begin
	IF exists (Select * from players where PlayerID = @PlayerID) -- je�li istnieje gracz, kt�ry chce sie zalogowa�
	begin
		if (Select count(*) from login_logs where @PlayerID=PlayerID) >= 20 -- je�li ilo�� zapisanych log�w jest >= 20 (w zasadzie max mo�e ona wynosi� 20)
		begin 
			declare @Date Date = (Select top 1 DateLogIn from login_logs where PlayerID = @PlayerID order by DateLogIn, TimeLogIn) -- wyszukujemy dat� najp�niejszego logowania gracza
			declare @Time time = (Select top 1 TimeLogIn from login_logs where PlayerID = @PlayerID order by DateLogIn, TimeLogIn) -- wyszukujemy czas najp�niejszego logowania gracza
			Delete from login_logs where PlayerID = @PlayerID and DateLogIn = @Date and TimeLogIn = @time -- usuwamy ten log
			insert into login_logs values (@PlayerID, @DateLogIn, @TimeLogIn, null, null) -- dodajemy nowy log
			fetch kursor into @PlayerID, @DateLogIn, @TimeLogIn, @DateLogOut, @TimeLogOut -- pobieramy kolejny wiersz z inserted
		end
		else
		begin -- je�li ilo�� log�w danego gracza jest < 20
			insert into login_logs values (@PlayerID, @DateLogIn, @TimeLogIn, null, null) -- dodajemy nowy log
			fetch kursor into @PlayerID, @DateLogIn, @TimeLogIn, @DateLogOut, @TimeLogOut -- pobieramy kolejny wiersz z inserted
		end
	end
	else
	begin
		Raiserror('Player does not exist',16,1)
		fetch kursor into @PlayerID, @DateLogIn, @TimeLogIn, @DateLogOut, @TimeLogOut
	end
end
close kursor
deallocate kursor
go

IF object_id ('Log_out') is not null -- procedura odpowiadaj�ca za wylogowanie gracza
drop proc Log_Out 
go
create proc Log_Out (@login nvarchar(30))
as
begin try
Declare @ID int = dbo.get_id(@login) -- wyszukujemy ID gracza
if Exists (Select * from login_logs where PlayerID = @ID and DateLogOut is null and TimeLogOut is null) -- Je�li gracz jest zalogowany
begin
	update login_logs -- updatujemy ostatni log
	Set DateLogOut = GetDate(),
	TimeLogOut = SYSDATETIME()
	where PlayerID = @ID and DateLogOut is null and TimeLogOut is null
	Print 'Player ' + @login + ' logs-out.'
end
else
begin
	Raiserror ('You are not logged-in',16,1)
end
end try
begin catch
	exec GetErrorInfo
end catch
go


If Object_id ('get_id') is not null -- funkcja kt�ra zwraca ID gracza o podanym jako parametr loginie
drop function get_id
go
create function get_id (@login Nvarchar(30))
returns int
as
begin
IF exists (Select PlayerID from players where login = @login)
	return (Select PlayerID from players where login = @login)
return -1
end 
go


If Object_id ('is_logged') is not null -- funkcja kt�ra zwraca 0 je�li gracz jest niezalogowany, oraz 1 gdy jest zalogowany
drop function is_logged
go
create function is_logged (@login Nvarchar(30))
returns bit
as
begin
declare @ID int = dbo.get_id(@login) -- znajdujemy ID gracza
IF exists (Select * from login_logs where PlayerID = @ID and DateLogOut is null and TimeLogOut is null) -- je�li jest zalogowany
	return 1
return 0
end
go

If Object_id ('is_logged2') is not null -- funkcja kt�ra zwraca 0 je�li gracz jest niezalogowany, oraz 1 gdy jest zalogowany
drop function is_logged2
go
create function is_logged2 (@ID Nvarchar(30))
returns bit
as
begin
IF exists (Select * from login_logs where PlayerID = @ID and DateLogOut is null and TimeLogOut is null) -- je�li jest zalogowany
	return 1
return 0
end
go


If Object_id ('check_premiums') is not null -- usuwa przedawnione konta premium
drop proc check_premiums
go

create proc check_premiums
as
delete from player_premium where GETDATE() > DateTo
go


If Object_id ('buy_premium') is not null -- procedura odpowiedzialna za kupienie konta premium
drop proc buy_premium
go
create proc buy_premium (@login Nvarchar(30), @type int, @time Nvarchar(10), @quantity int)
as
begin try
if @type not in (Select premiumID from premiums)
	Raiserror ('No such premium type',16,1)
If not exists (Select * from players where login = @login) -- je�li nie ma takiego konta w bazie
	Raiserror ('No account with this login',16,1)
IF @quantity <= 0 -- je�li podano z�� ilo�� tygodni/miesi�cy/lat
	Raiserror ('Quantity should be a positive number',16,1)
If @time not in ('week','month','year') -- je�li podano z�y okres czasu
	Raiserror ('Wrong period of time',16,1)

If @time = 'week'
begin
	If exists (Select * from player_premium where PlayerID = dbo.get_id(@login) and @type = PremiumID) -- je�li do tego konta przydzielono ju� premium
	begin
		update player_premium -- przed�u�amy czas konta premium
		Set DateTo = DateADD(day,7*@quantity,DateTo)
		where dbo.get_id(@login) = PlayerID and @type = PremiumID
	end
	else
	begin
		insert into player_premium values (dbo.get_id(@login),@type,GETDATE(),DateADD(day,7*@quantity,Getdate())) -- dodajemy nowe konto premium
	end
	Print 'Player ' + @login + ' bought a premium account of type ' + cast(@type as nvarchar) + ' for ' + cast(@quantity as nvarchar) + ' ' + @time + 's'
end

else if @time = 'month'
begin
	If exists (Select * from player_premium where PlayerID = dbo.get_id(@login) and @type = PremiumID) -- je�li do tego konta przydzielono ju� premium
	begin
		update player_premium -- przed�u�amy czas konta premium
		Set DateTo = DateADD(day,31*@quantity,DateTo)
		where dbo.get_id(@login) = PlayerID and @type = PremiumID
	end
	else
	begin
		insert into player_premium values (dbo.get_id(@login),@type,GETDATE(),DateADD(day,31*@quantity,Getdate())) -- dodajemy nowe konto premium
	end
	Print 'Player ' + @login + ' bought a premium account of type ' + cast(@type as nvarchar) + ' for ' + cast(@quantity as nvarchar) + ' ' + @time + 's'
end

else if @time = 'year'
begin
	If exists (Select * from player_premium where PlayerID = dbo.get_id(@login) and @type = PremiumID) -- je�li do tego konta przydzielono ju� premium
	begin
		update player_premium -- przed�u�amy czas konta premium
		Set DateTo = DateADD(year,@quantity,DateTo)
		where dbo.get_id(@login) = PlayerID and @type = PremiumID
	end
	else
	begin
		insert into player_premium values (dbo.get_id(@login),@type,GETDATE(),DateADD(year,@quantity,Getdate())) -- dodajemy nowe konto premium
	end
	Print 'Player ' + @login + ' bought a premium account of type ' + cast(@type as nvarchar) + ' for ' + cast(@quantity as nvarchar) + ' ' + @time + 's'
end
end try

begin catch
	exec GetErrorInfo
end catch
go

If object_id ('trigger_premium') is not null 
drop trigger trigger_premium
go
create trigger trigger_premium on player_premium -- przy ka�dym nowo wprowadzonym wierszu lub uaktualnieniu wiersza wywolujemy funkcj� check_premiums kt�ra usuwa przedawnione konta premium z tabeli
after update, insert
as
exec check_premiums
go

If object_id ('trigger_registration_logs') is not null 
drop trigger trigger_registration_logs
go
create trigger trigger_registration_logs on registration_logs
instead of delete
as
Print 'You can''t delete anything from table registration_logs.'
go

If object_id ('trigger_login_logs2') is not null 
drop trigger trigger_login_logs2
go
create trigger trigger_login_logs2 on login_logs
instead of delete
as
Print 'You can''t delete anything from table login_logs.'
go

If object_id ('delete_acc') is not null
drop proc delete_acc
go
create proc delete_acc (@login nvarchar(30), @password nvarchar(30)) -- procedura odpowiedzialna za usuni�cie konta
as
begin try
IF exists (Select * from players where Login = @login and Password = @password) -- je�li istnieje takie konto
begin
	IF dbo.is_logged(@login) = 1 -- je�li gracz zalogowany
	begin
	exec Log_Out @login -- wyloguj gracza
	end

	update registration_logs -- edytujemy log rejestracji - zapisujemy, kiedy konto zosta�o skasowane
	Set DeletionDate = GETDATE(),
	DeletionTime = SYSDATETIME()
	where PlayerID = dbo.get_id(@login)

	delete from players -- usuwamy konto
	where PlayerID = dbo.get_id(@login)
	Print 'Player ' + @login + ' deleted'
end
else
begin
	Raiserror('This account doesn''t exist.',16,1) -- je�li nie ma takiego konta
end
end try
begin catch
	exec GetErrorInfo
end catch
go

-- przyk�adowe widoki dla statystyk

IF object_id ('view_country_exists') is not null -- pokazuje procentowo z jakich kraj�w pochodz� gracze (posiadaj�cy konto)
drop view view_country_exists
go
create view view_country_exists as
Select Country, Count(*)*100/(Select count(*) from players) as [% of accounts] 
from registration_logs as R join players as P on R.PlayerID = P.PlayerID 
group by country
go

IF object_id ('view_country_not_exists') is not null -- pokazuje procentowo z jakich kraj�w pochodz� gracze (kt�rzy usun�li konto)
drop view view_country_not_exists
go
create view view_country_not_exists as
Select Country, Count(*)*100/((Select count(*) from registration_logs)-(Select count(*) from players)) as [% of accounts] 
from registration_logs as R left join players as P on R.PlayerID = P.PlayerID
where P.PlayerID is null
group by country
go

--Insert into premiums values (0.5,0) , (0,0.5) , (0.25,0.25) -- dodajemy 3 rodzaje konta premium
--Exec create_account 'root@root.com','rootroot','rootroot','rootroot','Root','1000-01-01' -- root b�dzie graczem o numerze 0
--Exec Log_In 'rootroot','rootroot' -- logujemy roota
-- ^ nale�y wykona� w 1 kolejno�ci po utworzeniu tabel, procedur itp.

-- jakie� testy:
/*Exec create_account 'rafik272@interia.pl','rafik272','hehehehe','hehehehe','Poland','1990-02-12'
Exec create_account 'rafik274@interia.pl','rafik274','hehehehe','hehehehe','Poland','1985-02-12'
Exec create_account 'rafik273@interia.pl','rafik273','hehehehe','hehehehe','Germany','1980-02-12'
Exec delete_acc 'rafik274','hehehehe'
Select * from registration_logs
Select * from view_country_exists
Select * from view_country_not_exists
*/
--delete from premiums
--Insert into premiums values (0.5,0) , (0,0.5) , (0.25,0.25)
/*Exec buy_premium 'rafik272',1,'week',3
Exec buy_premium 'rafik272',2,'year',3
Exec buy_premium 'rafik272',3,'month',2*/
/*Select * from players
Select * from premiums
Select * from player_premium
Select * from login_logs
Insert into login_logs values (0,GETDATE(),SYSDATETIME(),null,null), (6,GETDATE(),SYSDATETIME(),null,null)
exec Log_Out 'rafik272'
exec delete_acc 'rafik272', 'hehehehe'*/

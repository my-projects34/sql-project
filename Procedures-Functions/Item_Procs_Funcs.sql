/*
	Created: 31 January 2019

	Contains 
	1) 'Open_Lootbox' procedure

	Desc:
	1) This procedure performs an opening of the Lootbox item by viewing the available loot using cursor and choosing one record
		basing on a random generated number and a chance of the loot.
		In addidion the cursor is being opened on a table containing loot with randomly ordered records.
*/

IF OBJECT_ID('Open_Lootbox') IS NOT NULL
	DROP PROCEDURE Open_Lootbox

GO
CREATE PROCEDURE Open_Lootbox(@charID INT, @itemName VARCHAR(32), @result BIT OUTPUT)
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
		DECLARE @result1 BIT = 0
		IF EXISTS(SELECT * FROM Items WHERE Category = 'Loot Box' AND [Name] = @itemName)
		BEGIN
			EXEC TakeItem @charID, @itemName, 1, @result1 OUTPUT
			IF @result1 = 1
			BEGIN
				DECLARE @rand INT
				DECLARE @name VARCHAR(32)
				DECLARE @chance INT
				DECLARE Cur CURSOR FOR SELECT Item_Name, Chance FROM Loot WHERE Loot_Box_Name = @itemName ORDER BY NEWID()

				OPEN Cur
				FETCH Cur INTO @name, @chance

				WHILE @@FETCH_STATUS = 0
				BEGIN
					SET @rand = RAND()*(100)
					IF @chance >= @rand 
					BEGIN
						EXEC giveItem @charID, @name, 1, @result1 OUTPUT
						IF @result1 = 1
						BEGIN
							SET @result = 1
							BREAK
						END ELSE
						BEGIN
							RAISERROR('Could not give %s to %s.', 16, 1, @name, @charID)
						END
					END
					FETCH Cur INTO @name, @chance
				END

				IF @result = 0 -- if no item was assigned to the player in the cursor loop
				BEGIN
					SET @name = (SELECT TOP 1 Item_Name FROM Loot WHERE Loot_Box_Name = @itemName ORDER BY Chance ASC)
					EXEC giveItem @charID, @name, 1, @result1 OUTPUT
					IF @result1 = 1
					BEGIN
						SET @result = 1
					END ELSE
					BEGIN
						RAISERROR('Could not give %s to %s.', 16, 1, @name, @charID)
					END
				END

				CLOSE Cur
				DEALLOCATE Cur
			END ELSE
			BEGIN
				RAISERROR('Could not take %s from %s.', 16, 1, @name, @charID)
			END
		END ELSE
		BEGIN
			RAISERROR('%s does not exist.', 16, 1, @name)
		END
	
		IF @result = 1
		BEGIN
			COMMIT
		END ELSE
		BEGIN
			RAISERROR('result fail', 15, 1)
		END
	END TRY	
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK
			SET @result = 0
	END CATCH
END
GO
/*
	Created: 31 January 2019

	Contains:
	TradeLog table to store all performed trades, also the unsuccessful ones.
*/

IF OBJECT_ID('TradeLog') IS NOT NULL
	DROP TABLE TradeLog

CREATE TABLE TradeLog(
	ID INT PRIMARY KEY IDENTITY(1, 1) NOT NULL,
	CharName VARCHAR(32) NOT NULL,
	ItemName VARCHAR(32) NOT NULL,
	Amount INT NOT NULL,
	CharName2 VARCHAR(32) NOT NULL,
	ItemName2 VARCHAR(32) NOT NULL,
	Amount2 INT NOT NULL,

	Success BIT NOT NULL
)


create table registration_logs (
PlayerID int identity (0,1),
email Nvarchar(40) not null,
Country Nvarchar(30) not null,
BirthDate Date not null,
CreationDate Date not null,
CreationTime time not null,
DeletionDate Date null,
DeletionTime time null,
primary key (PlayerID)
)

create table players (
PlayerID int identity(0,1) not null,
Login Nvarchar(20) not null,
Password Nvarchar(20) not null,
Constraint FK_players Foreign Key (PlayerID) references registration_logs(PlayerID),
primary Key (PlayerID)
)

create table login_logs (
PlayerID int not null,
DateLogIn Date not null,
TimeLogIn time  not null,
DateLogOut Date null,
TimeLogOut time null,
primary key (PlayerID,Datelogin,TimeLogin)
)

/*
Je�li gracz ma DateLogOut null oraz TimeLogOut null,
to jest on aktualnie zalogowany na swoje konto w grze.
*/

create table premiums (
PremiumID int  identity (1,1) not null,
BonusGold float not null,
BonusExp float not null
primary key (PremiumID)
)

create table player_premium (
PlayerID int not null,
PremiumID int not null,
DateFrom Date not null,
DateTo Date not null
Constraint FK_player_premium1 Foreign Key (PlayerID) references Players(PlayerID)
ON DELETE CASCADE,
Constraint FK_player_premium2 Foreign Key (PremiumID) references premiums(PremiumID),
primary key (PlayerID,PremiumID)
)


--you need to run player and item tables first

if OBJECT_ID('[character outfit]') is not null
drop table [character outfit]

if OBJECT_ID('[character eq]') is not null
drop table [character eq]

if OBJECT_ID('[character stats]') is not null
drop table [character stats]

if OBJECT_ID('[character attributes]') is not null
drop table [character attributes]

if OBJECT_ID('[classes]') is not null
drop table [classes]

if OBJECT_ID('[character logs]') is not null
drop table [character logs]

if object_ID ('characters') is not null
drop table characters

create table characters (
	CharacterId  int not null identity,
	PlayerID int not null,
	CharacterName nvarchar(256) not null,
	Class nvarchar(32) not null,
	Race nvarchar(32) not null,
	Gender bit not null,
	[Level] int not null,
	Xp int not null,

	primary key (CharacterId),
	Constraint FK_characters Foreign Key (PlayerID) references players on delete cascade
)


create table [character attributes] (
	CharacterID int not null,
	Strength int not null,
	Dexternity int not null,
	Constitution int not null,
	Intelligence int not null,
	Wisdom int not null,
	Charisma int not null,

	primary key (CharacterID),
	Constraint [FK_character attributes] Foreign Key (CharacterID) references characters on delete cascade
)


create table [character stats] (
	CharacterID int not null,
	THAC0 int not null,
	BaseTHAC0 int not null,
	HP int not null,
	BaseHp int not null,
	ArmorClass int not null,
	BaseArmorClass int not null,
	Mana int not null,
	BaseMana int not null,
	EQSlots int not null,

	primary key (CharacterID),
	Constraint [FK_character stats] Foreign Key (CharacterID) references characters on delete cascade
)



create table [character eq] (
	CharacterID int not null,
	ItemName varchar(32) not null,
	Quanity int not null,

	Constraint [FK_character eq] Foreign Key (CharacterID) references characters on delete cascade
)



create table [character outfit] (
	CharacterID int not null,
	Head varchar(32),
	Body varchar(32),
	LeftArm varchar(32),
	RightArm varchar(32),
	Weapon varchar(32),
	Shield varchar(32),

	primary key (CharacterID),
	Constraint [FK_character outfit] Foreign Key (CharacterID) references characters on delete cascade
)

create table [character logs] (
	CharacterID int not null,
	[Date] time not null,
	[Description] varchar(4000) not null,

	primary key (CharacterID, [Date]),
	Constraint [FK_character logs] Foreign Key (CharacterID) references characters on delete cascade
)


create table classes (
	ClassName nvarchar(256),
	HPPerLvl int not null,
	ManaPerLvl int not null,
	THAC0PerLvl int not null,
	ArmorClassPerLvl int not null,

	Primary key (ClassName),
)

insert into classes  values
('Fighter', 10, 0, -2, -2),
('Ranger', 8, 1, -3, -1),
('Paladin', 9, 3, -1, -3),
('Cleric', 6, 7, -1, -1),
('Druid', 8, 5, -1, -1),
('Mage', 4, 10, -1, -1),
('Thief', 6, 0, -3, -1),
('Bard', 7, 4, -2, -1)


/*
	Created: 31 January 2019

	Contains 
	1) 'TradeRequest' procedure

	Desc:
	1) Performs a trade request using takeItem and giveItem procedures, then stores the information about a trade in trade logs table.
*/

IF OBJECT_ID('TradeRequest') IS NOT NULL
	DROP PROCEDURE TradeRequest

GO
CREATE PROCEDURE TradeRequest(@charID INT, @char2ID INT, @item1 VARCHAR(32), @amount1 INT, @item2 VARCHAR(32), @amount2 INT, @result BIT OUTPUT)
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
		DECLARE @result1 BIT = 0

		SET @result = 0

		DECLARE @player1ID INT
		DECLARE @player2ID INT

		SET @player1ID = (SELECT PlayerID FROM characters WHERE CharacterId = @charID)
		SET @player2ID = (SELECT PlayerID FROM characters WHERE CharacterId = @char2ID)

		IF (@player1ID IS NOT NULL) AND (@player2ID IS NOT NULL)
		BEGIN
			DECLARE @firstLogged BIT
			DECLARE @secondLogged BIT

			SET @firstLogged = dbo.is_logged2(@player1ID)
			SET @secondLogged = dbo.is_logged2(@player2ID)

			IF (@firstLogged = 1) AND (@secondLogged = 1)
			BEGIN
				EXEC takeItem @charID, @item1, @amount1, @result1 OUTPUT
				IF @result1 = 1
				BEGIN
					EXEC giveItem @charID, @item2, @amount2, @result1 OUTPUT
					IF (@result1 = 1)
					BEGIN
						EXEC takeItem @char2ID, @item2, @amount2, @result1 OUTPUT
						IF (@result1 = 1)
						BEGIN
							EXEC giveItem @char2ID, @item1, @amount1, @result1 OUTPUT
							IF (@result1 = 1)
							BEGIN
								SET @result = 1
								COMMIT
							END
						END
					END
				END	
			END		
		END

		IF @result = 0
		BEGIN
			ROLLBACK
			RAISERROR('Trade failed', 16, 1)
		END

		IF OBJECT_ID('TradeLog') IS NOT NULL
		BEGIN
			INSERT INTO TradeLog(CharName, ItemName, Amount, CharName2, ItemName2, Amount2, Success) VALUES
			(@charID, @item1, @amount1, @char2ID, @item2, @amount2, @result)
		END
	END TRY
	BEGIN CATCH
		SET @result = 0
		IF OBJECT_ID('TradeLog') IS NOT NULL
		BEGIN
			INSERT INTO TradeLog(CharName, ItemName, Amount, CharName2, ItemName2, Amount2, Success) VALUES
			(@charID, @item1, @amount1, @char2ID, @item2, @amount2, @result)
		END
	END CATCH
END
GO


--you need to run character utilities first

if OBJECT_ID('AddNPC') is not null
	drop proc AddNPC
go

create proc AddNPC (@NPCName nvarchar(256),  @class nvarchar(256),
			@race nvarchar(256), @gender bit, @strength int, @dexternity int, @constitution int,
			@intelligence int, @wisdom int, @charisma int )
as
	declare @rootID int
	set @rootID = 0
	exec AddCharacter @rootID, @NPCName, @class, @race, @gender, @strength, @dexternity, @constitution,
					@intelligence, @wisdom, @charisma
	update [character stats] set EQslots = 10000000 where characterID = (select CharacterId from characters where
				PlayerID = @rootID and CharacterName = @NPCName)
go

if OBJECT_ID('Refill') is not null
	drop proc Refill
go

create proc Refill(@NPCName nvarchar(256))
as
	declare @rootID int
	set @rootID = 0

	declare @NPCID int
	select @NPCID = CharacterId from characters where
				PlayerID = @rootID and CharacterName = @NPCName
	delete [character eq] where CharacterID = @NPCID

	declare i_ cursor
	for select CharacterID, ItemName, Quanity from [NPC Refill]
	for read only
	declare @tempCharacterID nvarchar(256), @tempItemName varchar(256)
	declare @tempQuanity int
	declare @garbage bit
	open i_
		fetch i_ into  @tempCharacterID, @tempItemName, @tempQuanity
		while @@FETCH_STATUS<>-1
		begin 
			if(@tempCharacterID = @NPCID)
			begin
				exec GiveItem @NPCID, @tempItemName, @tempQuanity, @garbage
			end

			fetch i_ into  @tempCharacterID, @tempItemName, @tempQuanity
		end
		close i_
		deallocate i_

go



/*
	Created: 30 January 2019

	- Check_Item_Tables PROCEDURE Checks whether all data in Item Tables is correct and items have their references in subtables if needed.
	- CategoriesToCheck TABLE is used as the index of categories to check.
*/

IF OBJECT_ID('CategoriesToCheck') IS NOT NULL
	DROP TABLE CategoriesToCheck

IF OBJECT_ID('Check_Item_Tables') IS NOT NULL
	DROP PROCEDURE Check_Item_Tables


CREATE TABLE CategoriesToCheck
(
	Category_Name VARCHAR(32) NOT NULL
)

INSERT INTO CategoriesToCheck VALUES
('Sword'),
('Wand'),
('Shield'),
('Armor'),
('Spell'),
('Food'),
('Amulet')


GO
CREATE PROCEDURE Check_Item_Tables(@Return_Status BIT OUTPUT)
AS
BEGIN
	SET @Return_Status = 1
	DECLARE @Name1 VARCHAR(32)
	DECLARE @Name2 VARCHAR(32)
	DECLARE @tempName VARCHAR(32)

	DECLARE @CAT_NAME VARCHAR(32)
	DECLARE GLOBAL_CURSOR CURSOR FOR SELECT * FROM CategoriesToCheck
	OPEN GLOBAL_CURSOR
	FETCH GLOBAL_CURSOR INTO @CAT_NAME
	WHILE @@FETCH_STATUS = 0
	BEGIN
		CREATE TABLE TempTable(
			Name1 VARCHAR(32),
			Name2 VARCHAR(32)
		)

		DECLARE @Querry NVARCHAR(512)
		SET @Querry = 
			'
			INSERT INTO TempTable
			SELECT I.Name, S.Name FROM Items AS I FULL JOIN ' + CONVERT(NVARCHAR(32), @CAT_NAME) + ' as S
			ON I.Name = S.Name
			WHERE Category = ''' + CONVERT(NVARCHAR(32), @CAT_NAME) + ''' and S.Name IS NULL OR I.Name IS NULL
			'
		
		EXEC sp_executesql @Querry
		
		DECLARE Cur CURSOR FOR SELECT * FROM TempTable
		OPEN Cur
		
		FETCH Cur INTO @Name1, @Name2

		WHILE @@FETCH_STATUS = 0 
		BEGIN
			IF (@Name1 IS NULL)
				SET @Return_Status = 0
				PRINT(@Name2 + ' is missing in the items table.')
			IF (@Name2 IS NULL)
				SET @Return_Status = 0
				PRINT(@Name1 + ' is missing in the ' + CONVERT(NVARCHAR(32), @CAT_NAME) + ' subtable.')
			FETCH Cur INTO @Name1, @Name2
		END

		CLOSE Cur
		DEALLOCATE Cur

		DROP TABLE TempTable
		FETCH GLOBAL_CURSOR INTO @CAT_NAME
	END

	CLOSE GLOBAL_CURSOR
	DEALLOCATE GLOBAL_CURSOR

	

	
	IF @Return_Status = 1
	BEGIN
		PRINT('Item tables are complete.')
	END
END
GO


/*
	Created: 31 January 2019

	Contains 
	1) 'Open_Lootbox' procedure

	Desc:
	1) This procedure performs an opening of the Lootbox item by viewing the available loot using cursor and choosing one record
		basing on a random generated number and a chance of the loot.
		In addidion the cursor is being opened on a table containing loot with randomly ordered records.
*/

IF OBJECT_ID('Open_Lootbox') IS NOT NULL
	DROP PROCEDURE Open_Lootbox

GO
CREATE PROCEDURE Open_Lootbox(@charID INT, @itemName VARCHAR(32), @result BIT OUTPUT)
AS
BEGIN
	BEGIN TRANSACTION
	BEGIN TRY
		DECLARE @result1 BIT = 0
		IF EXISTS(SELECT * FROM Items WHERE Category = 'Loot Box' AND [Name] = @itemName)
		BEGIN
			EXEC TakeItem @charID, @itemName, 1, @result1 OUTPUT
			IF @result1 = 1
			BEGIN
				DECLARE @rand INT
				DECLARE @name VARCHAR(32)
				DECLARE @chance INT
				DECLARE Cur CURSOR FOR SELECT Item_Name, Chance FROM Loot WHERE Loot_Box_Name = @itemName ORDER BY NEWID()

				OPEN Cur
				FETCH Cur INTO @name, @chance

				WHILE @@FETCH_STATUS = 0
				BEGIN
					SET @rand = RAND()*(100)
					IF @chance >= @rand 
					BEGIN
						EXEC giveItem @charID, @name, 1, @result1 OUTPUT
						IF @result1 = 1
						BEGIN
							SET @result = 1
							BREAK
						END ELSE
						BEGIN
							RAISERROR('Could not give %s to %s.', 16, 1, @name, @charID)
						END
					END
					FETCH Cur INTO @name, @chance
				END

				IF @result = 0 -- if no item was assigned to the player in the cursor loop
				BEGIN
					SET @name = (SELECT TOP 1 Item_Name FROM Loot WHERE Loot_Box_Name = @itemName ORDER BY Chance ASC)
					EXEC giveItem @charID, @name, 1, @result1 OUTPUT
					IF @result1 = 1
					BEGIN
						SET @result = 1
					END ELSE
					BEGIN
						RAISERROR('Could not give %s to %s.', 16, 1, @name, @charID)
					END
				END

				CLOSE Cur
				DEALLOCATE Cur
			END ELSE
			BEGIN
				RAISERROR('Could not take %s from %s.', 16, 1, @name, @charID)
			END
		END ELSE
		BEGIN
			RAISERROR('%s does not exist.', 16, 1, @name)
		END
	
		IF @result = 1
		BEGIN
			COMMIT
		END ELSE
		BEGIN
			RAISERROR('result fail', 15, 1)
		END
	END TRY	
	BEGIN CATCH
		IF @@TRANCOUNT > 0
			ROLLBACK
			SET @result = 0
	END CATCH
END
GO


IF OBJECT_ID ('GetErrorInfo') IS NOT NULL
	DROP PROCEDURE GetErrorInfo

IF OBJECT_ID('ErrorDisplayer') IS NOT NULL
	DROP PROCEDURE ErrorDisplayer

GO
CREATE PROCEDURE GetErrorInfo
AS
 SELECT
 ERROR_NUMBER() AS ErrorNumber,
 ERROR_SEVERITY() AS ErrorSeverity,
 ERROR_STATE() AS ErrorState,
 ERROR_PROCEDURE() AS ErrorProcedure,
 ERROR_LINE() AS ErrorLine,
 ERROR_MESSAGE() AS ErrorMessage;
GO

GO
CREATE PROCEDURE ErrorDisplayer (@MSG NVARCHAR(128), @SEVERITY INT = 16, @STATE INT = 1)
AS
	RAISERROR(@MSG, @SEVERITY, @STATE)
GO



If object_Id ('create_account') is not null
drop proc create_account
go
Create proc create_account (@email Nvarchar(40), @login Nvarchar(30), @password Nvarchar(30), @password2 Nvarchar(30), @country Nvarchar(30), @BirthDate Date) -- tworzenie konta
as 
begin try
IF @email is null or @login is null or @password is null or @password2 is null or @country is null or @BirthDate is null -- je�li jaki� parametr nie zosta� podany
begin
	Raiserror ('Fill in the empty field',16,1)
end
IF ((Select CHARINDEX('@',@email)) = 0 or (Select CHARINDEX('.',@email)) = 0) -- je�li w emailu brakuje "." lub "@"
begin
	Raiserror ('Wrong email',16,1)
end
IF exists (Select * from registration_logs as R join players as P on R.PlayerID = P.PlayerID where email = @email) -- je�li ten email ju� jest przypisany do jakiego� konta
begin
	Raiserror ('This email adress is already in the database',16,1)
end
IF (Len(@login)<8 or Len(@login)>20) -- je�li login nie mie�ci si� w zakresie d�ugo�ci [8;20]
begin
	Raiserror ('Login''s length is not correct',16,1)
end
IF exists (Select * from players where Login = @login) -- je�li login jest ju� zaj�ty
begin
	Raiserror('This login has been already taken',16,1)
end
IF (Len(@password)<8 or Len(@password)>20) -- je�li has�o nie mie�ci si� w zakresie [8;20]
begin
	Raiserror ('Password''s length is not correct',16,1)
end
IF @password <> @password2 -- je�li powt�rzenie has�a jest inne ni� pierwsze podane has�o
begin
	Raiserror('Second password entered incorrectly',16,1)
end
IF (Datediff(dd,@BirthDate,GETDATE())/365) < 18 -- je�li osoba nie ma uko�czonych 18 lat
begin
	Raiserror('You are under 18',16,1)
end

insert into registration_logs values (@email, @country, @BirthDate, GetDate(), SYSDATETIME(), null, null) -- dodajemy logi rejestracji odpowiedniego gracza
insert into players values (@login,@password) -- dodajemy osob� do tabeli players

Print 'Account ' + @login + ' created.'
end try

begin catch
	exec GetErrorInfo
end catch
Go

IF object_id ('Log_In') is not null
drop proc Log_In 
go
create procedure Log_In (@login Nvarchar(30), @password Nvarchar(30)) -- logowanie si� do gry
as
begin try
declare @ID int -- do wyszukiwania w logach_logowania
if exists (Select * from players where Login = @login and Password = @password) -- Je�li w bazie znajduje si� taki gracz
begin
	Set @ID = dbo.get_id(@login) -- u�ywamy pomocniczej funkcji
	IF dbo.is_logged(@login) = 0 -- Je�li gracz nie jest aktualnie zalogowany
	begin
		insert into login_logs values (@ID,GetDate(),SYSDATETIME(),null,null) -- logujemy gracza
		Print 'Player ' + @login + ' logs-in.'
	end
	else
	begin
		Raiserror ('You are currently logged-in',16,1)
	end
end
else
begin 
	Raiserror('Your account doesn''t exist. Check your login or password',16,1) -- je�li w bazie nie ma takiego gracza
end
end try
begin catch
	exec GetErrorInfo
end catch
go

If object_id('insert_login_logs') is not null -- trigger kt�ry odpowiada za kontolowanie dodawania log�w, maksymalnie przechowywanych jest 20 ostatnich logowa� ka�dego gracza.
drop trigger insert_login_logs
go
create trigger insert_login_logs
on login_logs instead of insert
as
declare @PlayerID int
declare @DateLogIn Date
declare @TimeLogIn time
declare @DateLogOut Date
declare @TimeLogOut time
declare kursor cursor for Select * from inserted -- kursor na tabeli inserted
for read only
open kursor
fetch kursor into @PlayerID, @DateLogIn, @TimeLogIn, @DateLogOut, @TimeLogOut -- pobieramy 1 wiersz
while @@FETCH_STATUS <> -1 -- dop�ki istniej� wiersze w tabeli inserted
begin
	IF exists (Select * from players where PlayerID = @PlayerID) -- je�li istnieje gracz, kt�ry chce sie zalogowa�
	begin
		if (Select count(*) from login_logs where @PlayerID=PlayerID) >= 20 -- je�li ilo�� zapisanych log�w jest >= 20 (w zasadzie max mo�e ona wynosi� 20)
		begin 
			declare @Date Date = (Select top 1 DateLogIn from login_logs where PlayerID = @PlayerID order by DateLogIn, TimeLogIn) -- wyszukujemy dat� najp�niejszego logowania gracza
			declare @Time time = (Select top 1 TimeLogIn from login_logs where PlayerID = @PlayerID order by DateLogIn, TimeLogIn) -- wyszukujemy czas najp�niejszego logowania gracza
			Delete from login_logs where PlayerID = @PlayerID and DateLogIn = @Date and TimeLogIn = @time -- usuwamy ten log
			insert into login_logs values (@PlayerID, @DateLogIn, @TimeLogIn, null, null) -- dodajemy nowy log
			fetch kursor into @PlayerID, @DateLogIn, @TimeLogIn, @DateLogOut, @TimeLogOut -- pobieramy kolejny wiersz z inserted
		end
		else
		begin -- je�li ilo�� log�w danego gracza jest < 20
			insert into login_logs values (@PlayerID, @DateLogIn, @TimeLogIn, null, null) -- dodajemy nowy log
			fetch kursor into @PlayerID, @DateLogIn, @TimeLogIn, @DateLogOut, @TimeLogOut -- pobieramy kolejny wiersz z inserted
		end
	end
	else
	begin
		Raiserror('Player does not exist',16,1)
		fetch kursor into @PlayerID, @DateLogIn, @TimeLogIn, @DateLogOut, @TimeLogOut
	end
end
close kursor
deallocate kursor
go

IF object_id ('Log_out') is not null -- procedura odpowiadaj�ca za wylogowanie gracza
drop proc Log_Out 
go
create proc Log_Out (@login nvarchar(30))
as
begin try
Declare @ID int = dbo.get_id(@login) -- wyszukujemy ID gracza
if Exists (Select * from login_logs where PlayerID = @ID and DateLogOut is null and TimeLogOut is null) -- Je�li gracz jest zalogowany
begin
	update login_logs -- updatujemy ostatni log
	Set DateLogOut = GetDate(),
	TimeLogOut = SYSDATETIME()
	where PlayerID = @ID and DateLogOut is null and TimeLogOut is null
	Print 'Player ' + @login + ' logs-out.'
end
else
begin
	Raiserror ('You are not logged-in',16,1)
end
end try
begin catch
	exec GetErrorInfo
end catch
go


If Object_id ('get_id') is not null -- funkcja kt�ra zwraca ID gracza o podanym jako parametr loginie
drop function get_id
go
create function get_id (@login Nvarchar(30))
returns int
as
begin
IF exists (Select PlayerID from players where login = @login)
	return (Select PlayerID from players where login = @login)
return -1
end 
go


If Object_id ('is_logged') is not null -- funkcja kt�ra zwraca 0 je�li gracz jest niezalogowany, oraz 1 gdy jest zalogowany
drop function is_logged
go
create function is_logged (@login Nvarchar(30))
returns bit
as
begin
declare @ID int = dbo.get_id(@login) -- znajdujemy ID gracza
IF exists (Select * from login_logs where PlayerID = @ID and DateLogOut is null and TimeLogOut is null) -- je�li jest zalogowany
	return 1
return 0
end
go

If Object_id ('is_logged2') is not null -- funkcja kt�ra zwraca 0 je�li gracz jest niezalogowany, oraz 1 gdy jest zalogowany
drop function is_logged2
go
create function is_logged2 (@ID Nvarchar(30))
returns bit
as
begin
IF exists (Select * from login_logs where PlayerID = @ID and DateLogOut is null and TimeLogOut is null) -- je�li jest zalogowany
	return 1
return 0
end
go


If Object_id ('check_premiums') is not null -- usuwa przedawnione konta premium
drop proc check_premiums
go

create proc check_premiums
as
delete from player_premium where GETDATE() > DateTo
go


If Object_id ('buy_premium') is not null -- procedura odpowiedzialna za kupienie konta premium
drop proc buy_premium
go
create proc buy_premium (@login Nvarchar(30), @type int, @time Nvarchar(10), @quantity int)
as
begin try
if @type not in (Select premiumID from premiums)
	Raiserror ('No such premium type',16,1)
If not exists (Select * from players where login = @login) -- je�li nie ma takiego konta w bazie
	Raiserror ('No account with this login',16,1)
IF @quantity <= 0 -- je�li podano z�� ilo�� tygodni/miesi�cy/lat
	Raiserror ('Quantity should be a positive number',16,1)
If @time not in ('week','month','year') -- je�li podano z�y okres czasu
	Raiserror ('Wrong period of time',16,1)

If @time = 'week'
begin
	If exists (Select * from player_premium where PlayerID = dbo.get_id(@login) and @type = PremiumID) -- je�li do tego konta przydzielono ju� premium
	begin
		update player_premium -- przed�u�amy czas konta premium
		Set DateTo = DateADD(day,7*@quantity,DateTo)
		where dbo.get_id(@login) = PlayerID and @type = PremiumID
	end
	else
	begin
		insert into player_premium values (dbo.get_id(@login),@type,GETDATE(),DateADD(day,7*@quantity,Getdate())) -- dodajemy nowe konto premium
	end
	Print 'Player ' + @login + ' bought a premium account of type ' + cast(@type as nvarchar) + ' for ' + cast(@quantity as nvarchar) + ' ' + @time + 's'
end

else if @time = 'month'
begin
	If exists (Select * from player_premium where PlayerID = dbo.get_id(@login) and @type = PremiumID) -- je�li do tego konta przydzielono ju� premium
	begin
		update player_premium -- przed�u�amy czas konta premium
		Set DateTo = DateADD(day,31*@quantity,DateTo)
		where dbo.get_id(@login) = PlayerID and @type = PremiumID
	end
	else
	begin
		insert into player_premium values (dbo.get_id(@login),@type,GETDATE(),DateADD(day,31*@quantity,Getdate())) -- dodajemy nowe konto premium
	end
	Print 'Player ' + @login + ' bought a premium account of type ' + cast(@type as nvarchar) + ' for ' + cast(@quantity as nvarchar) + ' ' + @time + 's'
end

else if @time = 'year'
begin
	If exists (Select * from player_premium where PlayerID = dbo.get_id(@login) and @type = PremiumID) -- je�li do tego konta przydzielono ju� premium
	begin
		update player_premium -- przed�u�amy czas konta premium
		Set DateTo = DateADD(year,@quantity,DateTo)
		where dbo.get_id(@login) = PlayerID and @type = PremiumID
	end
	else
	begin
		insert into player_premium values (dbo.get_id(@login),@type,GETDATE(),DateADD(year,@quantity,Getdate())) -- dodajemy nowe konto premium
	end
	Print 'Player ' + @login + ' bought a premium account of type ' + cast(@type as nvarchar) + ' for ' + cast(@quantity as nvarchar) + ' ' + @time + 's'
end
end try

begin catch
	exec GetErrorInfo
end catch
go

If object_id ('trigger_premium') is not null 
drop trigger trigger_premium
go
create trigger trigger_premium on player_premium -- przy ka�dym nowo wprowadzonym wierszu lub uaktualnieniu wiersza wywolujemy funkcj� check_premiums kt�ra usuwa przedawnione konta premium z tabeli
after update, insert
as
exec check_premiums
go

If object_id ('trigger_registration_logs') is not null 
drop trigger trigger_registration_logs
go
create trigger trigger_registration_logs on registration_logs
instead of delete
as
Print 'You can''t delete anything from table registration_logs.'
go

If object_id ('trigger_login_logs2') is not null 
drop trigger trigger_login_logs2
go
create trigger trigger_login_logs2 on login_logs
instead of delete
as
Print 'You can''t delete anything from table login_logs.'
go

If object_id ('delete_acc') is not null
drop proc delete_acc
go
create proc delete_acc (@login nvarchar(30), @password nvarchar(30)) -- procedura odpowiedzialna za usuni�cie konta
as
begin try
IF exists (Select * from players where Login = @login and Password = @password) -- je�li istnieje takie konto
begin
	IF dbo.is_logged(@login) = 1 -- je�li gracz zalogowany
	begin
	exec Log_Out @login -- wyloguj gracza
	end

	update registration_logs -- edytujemy log rejestracji - zapisujemy, kiedy konto zosta�o skasowane
	Set DeletionDate = GETDATE(),
	DeletionTime = SYSDATETIME()
	where PlayerID = dbo.get_id(@login)

	delete from players -- usuwamy konto
	where PlayerID = dbo.get_id(@login)
	Print 'Player ' + @login + ' deleted'
end
else
begin
	Raiserror('This account doesn''t exist.',16,1) -- je�li nie ma takiego konta
end
end try
begin catch
	exec GetErrorInfo
end catch
go

-- przyk�adowe widoki dla statystyk

IF object_id ('view_country_exists') is not null -- pokazuje procentowo z jakich kraj�w pochodz� gracze (posiadaj�cy konto)
drop view view_country_exists
go
create view view_country_exists as
Select Country, Count(*)*100/(Select count(*) from players) as [% of accounts] 
from registration_logs as R join players as P on R.PlayerID = P.PlayerID 
group by country
go

IF object_id ('view_country_not_exists') is not null -- pokazuje procentowo z jakich kraj�w pochodz� gracze (kt�rzy usun�li konto)
drop view view_country_not_exists
go
create view view_country_not_exists as
Select Country, Count(*)*100/((Select count(*) from registration_logs)-(Select count(*) from players)) as [% of accounts] 
from registration_logs as R left join players as P on R.PlayerID = P.PlayerID
where P.PlayerID is null
group by country
go

--Insert into premiums values (0.5,0) , (0,0.5) , (0.25,0.25) -- dodajemy 3 rodzaje konta premium
--Exec create_account 'root@root.com','rootroot','rootroot','rootroot','Root','1000-01-01' -- root b�dzie graczem o numerze 0
--Exec Log_In 'rootroot','rootroot' -- logujemy roota
-- ^ nale�y wykona� w 1 kolejno�ci po utworzeniu tabel, procedur itp.

-- jakie� testy:
/*Exec create_account 'rafik272@interia.pl','rafik272','hehehehe','hehehehe','Poland','1990-02-12'
Exec create_account 'rafik274@interia.pl','rafik274','hehehehe','hehehehe','Poland','1985-02-12'
Exec create_account 'rafik273@interia.pl','rafik273','hehehehe','hehehehe','Germany','1980-02-12'
Exec delete_acc 'rafik274','hehehehe'
Select * from registration_logs
Select * from view_country_exists
Select * from view_country_not_exists
*/
--delete from premiums
--Insert into premiums values (0.5,0) , (0,0.5) , (0.25,0.25)
/*Exec buy_premium 'rafik272',1,'week',3
Exec buy_premium 'rafik272',2,'year',3
Exec buy_premium 'rafik272',3,'month',2*/
/*Select * from players
Select * from premiums
Select * from player_premium
Select * from login_logs
Insert into login_logs values (0,GETDATE(),SYSDATETIME(),null,null), (6,GETDATE(),SYSDATETIME(),null,null)
exec Log_Out 'rafik272'
exec delete_acc 'rafik272', 'hehehehe'*/



--you need to run character tables first

if object_ID ('NPC Refill') is not null
drop table [NPC Refill]

create table [NPC Refill] (
	CharacterId  int,
	ItemName varchar(32) not null,
	Quanity int not null,

	Constraint [FK_NPC Refill] Foreign Key (CharacterID) references characters on delete cascade
)



--you need to run Character_Tables.sql first


if OBJECT_ID('AddCharacter') is not null
	drop proc AddCharacter
go

create proc AddCharacter(@playerID int, @characterName nvarchar(256),  @class nvarchar(256),
			@race nvarchar(256), @gender bit, @strength int, @dexternity int, @constitution int,
			@intelligence int, @wisdom int, @charisma int )
as
	begin try
		declare @error nvarchar(256) 
		if( not exists ( select *from players where players.PlayerID = @playerID))
		begin
			set @error = 'Player ' + cast (@playerID as nvarchar) + ' does not exists' 
			raiserror (@error, 16, 1)
		end

		if (exists ( select * from characters  as c where c.CharacterName = @characterName and c.PlayerID = @playerID) )
		begin
			set @error =  'Player ' + cast (@playerID as nvarchar) + ' already have character named ' + @characterName 
			raiserror (@error, 16, 1)
		end

		if (not exists (select * from classes where ClassName = @class ) )
		begin 
			set @error = 'Class ' + @class + ' does not exists'
			raiserror (@error, 16, 1)
		end

		insert into characters values
		(@playerID, @characterName, @class, @race, @gender, 1, 0)
		declare @characterID int
		select @characterID =CharacterID from characters as c where c.PlayerID = @playerID and c.CharacterName = @characterName
		
		insert into [character attributes] values
		(@characterID, @strength, @dexternity, @constitution, @intelligence, @wisdom, @charisma)

		insert into [character stats] values
		(@characterID, 20, 20, 10, 10, 9, 9, 10, 10, 20)

		insert into [character logs] values
		(@characterID, GETDATE(), 'New character is made')
	end try

	begin catch
		exec GetErrorInfo
	end catch
go

if OBJECT_ID('GiveItem') is not null
	drop proc GiveItem
go

create proc GiveItem(@characterID int, @itemName varchar(32), @quanity int, @result bit OUT)
as
	begin transaction
	begin try
		declare @error nvarchar(256) 
		if( not exists ( select *from characters where characters.CharacterId = @characterID))
		begin
			set @error = 'Character ' + cast (@characterID as nvarchar) + ' does not exists' 
			raiserror (@error, 16, 1)
		end

		if( not exists ( select *from items where Items.Name = @itemName))
		begin
			set @error = 'Item ' + @itemName + ' does not exists' 
			raiserror (@error, 16, 1)
		end

		declare i cursor
		for select CharacterID, ItemName, Quanity from [character eq]
		for update of Quanity
		declare @tempCharacterID nvarchar(256), @tempItemName varchar(256)
		declare @tempQuanity int
		declare @itemStackMax int
		declare @maxSlots int
		declare @currentSlots int
		set @currentSlots = 0
		select @itemStackMax = items.MaxStack from items where Items.Name = @itemName
		select @maxSlots =[character stats].EQSlots from [character stats]

		open i
		fetch i into  @tempCharacterID, @tempItemName, @tempQuanity
		while (@@FETCH_STATUS<>-1 and @quanity>0)
		begin 
			if(@tempCharacterID = @characterID)
			begin
				set @currentSlots = @currentSlots +1 --duh
				if(@tempItemName = @itemName)
				begin
					if(@tempQuanity < @itemStackMax)
					begin 
						declare @tempStack int
						set @tempStack = @itemStackMax - @tempQuanity
						set @quanity = @quanity - @tempStack
						if(@quanity >= 0)
						begin
							update [character eq] set Quanity = Quanity + @tempStack where current of i
						end
						else
						begin
							set @quanity = @quanity + @tempStack
							update [character eq] set Quanity = Quanity + @quanity where current of i
							set @quanity = 0
						end
					end
				end
			end


			if(@currentSlots = @maxSlots)
			begin
				
				raiserror('not enough place in eq', 16, 1)
			end

			fetch i into  @tempCharacterID, @tempItemName, @tempQuanity
		end
		close i
		deallocate i

		if(@quanity > 0)
		--that means, that we didnt put everything on the stacks we already had
		begin
			while (@quanity >0)
			begin
			if(@currentSlots = @maxSlots)
				begin
					raiserror('not enough place in eq', 16, 1)
				end

				if(@quanity > @itemStackMax)
				begin
					set @quanity = @quanity - @itemStackMax
					insert into [character eq] values
					(@characterID, @itemName, @itemStackMax)
				end
				else
				begin
					insert into [character eq] values
					(@characterID, @itemName, @quanity)
					set @quanity = 0
				end
			end
		end

		set @result = 1
		if @@trancount > 0 commit transaction
	end try
	begin catch
		set @result = 0 
		exec GetErrorInfo
		if @@trancount > 0 rollback transaction
	end catch
go


if OBJECT_ID('TakeItem') is not null
	drop proc TakeItem
go

create proc TakeItem(@characterID int, @itemName varchar(32), @quanity int, @result bit OUT)
as
	begin transaction
	begin try
		declare @error nvarchar(256) 
		if( not exists ( select *from characters where characters.CharacterId = @characterID))
		begin
			set @error = 'Character ' + cast (@characterID as nvarchar) + ' does not exists' 
			raiserror (@error, 16, 1)
		end

		if( not exists ( select *from items where Items.Name = @itemName))
		begin
			set @error = 'Item ' + @itemName + ' does not exists' 
			raiserror (@error, 16, 1)
		end


		declare i cursor
		for select CharacterID, ItemName, Quanity from [character eq]
		for update of Quanity
		declare @tempCharacterID nvarchar(256), @tempItemName varchar(256)
		declare @tempQuanity int

		open i
		fetch i into  @tempCharacterID, @tempItemName, @tempQuanity
		while (@@FETCH_STATUS<>-1 and @quanity >0)
		begin 
			if(@tempCharacterID = @characterID)
			begin
				if(@tempItemName = @itemName)
				begin
					if(@tempQuanity>@quanity)
					begin
						update [character eq] set Quanity = Quanity - @quanity where current of i
						set @quanity = 0
					end
					else
					begin
						set @quanity = @quanity - @tempQuanity
						delete from [character eq] where current of i
					end
				end
			end
			fetch i into  @tempCharacterID, @tempItemName, @tempQuanity
		end
		close i
		deallocate i
		if(@quanity > 0)
		begin
			set @error = 'Character doesn''t have enough of item ' + @itemName
			raiserror(@error, 16, 1)
		end

		set @result = 1
		if @@trancount > 0 commit transaction
	end try
	begin catch
		set @result = 0 
		exec GetErrorInfo
		if @@trancount > 0 rollback transaction
	end catch
go

if OBJECT_ID('Reload') is not null
	drop proc Reload
go

create proc Reload(@characterID int)
as
	update [character stats] set ArmorClass = BaseArmorClass, HP = BaseHp, Mana = BaseMana,
	THAC0 = BaseTHAC0 where CharacterID = @characterID
--here we reload character stats
--basically we make for example ArmorClass = BaseArmorClass
--and the get the modifiers from armour and stuff
--same with everything else in stats
go


if OBJECT_ID('LevelUp') is not null
	drop proc LevelUp
go

create proc LevelUp(@characterID int)
as

	declare @class nvarchar(256)
	select @class = class from characters where CharacterId = @characterID
	declare @HPPerLvl int, @ManaPerLvl int
	declare @THAC0PerLvl int, @ArmorClassPerLvl int
	select @HPPerLvl= HPPerLvl, @ManaPerLvl = ManaPerLvl , 
			@THAC0PerLvl =THAC0PerLvl, @ArmorClassPerLvl = ArmorClassPerLvl 
			from classes where ClassName = @class
	update [character stats] set BaseArmorClass = BaseArmorClass + @ArmorClassPerLvl,
			BaseHp = BaseHp + @HPPerLvl, BaseMana = BaseMana + @ManaPerLvl, BaseTHAC0 = BaseTHAC0 + @THAC0PerLvl
	where CharacterId = @characterID
	Exec Reload @characterID


	declare @message nvarchar(256)
	declare @lvl int
	select @lvl = [level] from characters where CharacterId = @characterID
	set @message = 'level up ( from ' + cast(@lvl as nvarchar) + ' to ' + cast(@lvl+1 as nvarchar) + ' )' 
	insert into [character logs] values
	(@characterID, GETDATE(), @message)
go


if OBJECT_ID('GetGold') is not null
	drop proc GetGold
go

create proc GetGold(@money int, @characterID int)
as
	declare @trash bit
	declare @playerID int
	select @playerID = PlayerID from characters where characterID = @characterID
	if exists (Select * from player_premium where PlayerID = @playerID)
	begin
		declare @premium_id int = (Select PremiumID from player_premium where PlayerID = @playerID)
		set @money = @money*(1+(Select SUM(BonusGold) from premiums where PremiumID = @premium_id))
	end 
	exec GiveItem @characterID, 'Gold Coin',@money, @trash
go


if OBJECT_ID('GetXP') is not null
	drop proc GetXP
go

create proc GetXP(@exp int, @characterID int)
as
	declare @trash bit
	declare @playerID int
	select @playerID = PlayerID from characters where characterID = @characterID
	if exists (Select * from player_premium where PlayerID = @playerID)
	begin
		declare @premium_id int = (Select PremiumID from player_premium where PlayerID = @playerID)
		set @exp = @exp*(1+(Select SUM(BonusExp) from premiums where PremiumID = @premium_id))
	end 
	update characters set Xp = @exp where CharacterId = @characterID
go


--that's only for creating for accounts
Exec create_account 'root@root.root', 'rootroot', 'rootroot', 'rootroot', 'root', '2000-03-25'
Exec create_account 'rafik272@interia.pl','rafik272','heheloler','heheloler','Poland','2000-02-15'
Exec create_account 'rafik273@interia.pl','rafik273','heheloler','heheloler','Poland','2000-02-15'
Exec create_account 'rafik274@interia.pl','rafik274','heheloler','heheloler','Poland','2000-02-15'
Exec create_account 'rafik275@interia.pl','rafik275','heheloler','heheloler','Poland','2000-02-15'


Exec create_account 'admin@aureo.com','Kacprate','123456789','123456789','World','1970-01-01'

exec AddCharacter 1, 'Ajanatis', 'Paladin', 'Human', 1, 17, 13, 16, 12, 13, 17
exec AddCharacter 1, 'Alora', 'Thief','Halfling', 0, 8, 19, 12, 14, 7, 10
exec AddCharacter 1, 'Branwen', 'Cleric','Human', 0, 13, 16, 15, 9, 16, 13
exec AddCharacter 2, 'Coran', 'Thief','Elf', 1, 14, 20, 12, 14, 9, 16 
exec AddCharacter 2, 'Dynaheir', 'Mage','Human', 0, 11, 13, 16, 17, 15, 12 
exec AddCharacter 3, 'Edwin', 'Mage','Human', 1, 9, 10, 16, 18, 9, 10
exec AddCharacter 4, 'Eldoth', 'Bard','Human', 1, 16, 12, 15, 13, 10, 16
exec AddCharacter 4, 'Faldorn', 'Druid','Human', 0, 12, 15, 11, 10, 16, 15
exec AddCharacter 4, 'Garrick', 'Bard','Human', 1, 14, 16, 9, 13, 14, 15    
exec AddCharacter 4, 'Imoen', 'Thief','Human', 0, 9, 18, 16, 17, 11, 16 
exec AddCharacter 4, 'Jaheira', 'Druid','Human', 0, 15, 14, 17, 10, 14, 15 
exec AddCharacter 4, 'Kagain', 'Fighter','Dwafr', 1, 16, 12, 20, 15, 11, 8 
exec AddCharacter 4, 'Khalid', 'Fighter','Half-Elf', 1, 15, 16, 17, 12, 10, 9 
exec AddCharacter 4, 'Kivan', 'Ranger','Elf', 1, 18, 17, 14, 10, 14, 8 
exec AddCharacter 4, 'Minsc', 'Ranger','Human', 1, 18, 15, 15, 8, 6, 9 
exec AddCharacter 4, 'Montaron', 'Thief','Halfing', 1, 16, 17, 15, 12, 13, 9 
exec AddCharacter 4, 'Quayle', 'Mage','Gnome', 1, 8, 15, 11, 17, 10, 6
exec AddCharacter 4, 'Safana', 'Thief','Human', 0, 13, 17, 12, 16, 9, 17 
exec AddCharacter 4, 'Shar-Teel', 'Fighter','Human', 0, 18, 17, 9, 14, 7, 11
exec AddCharacter 4, 'Skie', 'Thief','Human', 0, 11, 18, 15, 15, 8, 13
exec AddCharacter 4, 'Tiax', 'Cleric','Gnome', 1, 9, 16, 16, 10, 12, 9  
exec AddCharacter 4, 'Viconia', 'Cleric','Drow', 0, 10, 19, 8, 16, 15, 14 
exec AddCharacter 4, 'Xan', 'Mage','Elf', 1, 13, 16, 7, 17, 14, 16 
exec AddCharacter 4, 'Xzar', 'Mage','Human', 1, 14, 16, 10, 17, 16, 10 
exec AddCharacter 4, 'Yeslick', 'Fighter','Dwarf', 1, 15, 13, 17, 7, 16, 10 

/*select * from characters
select * from [character stats]
select * from [character outfit]
select * from [character logs]
select * from [character attributes]
select * from [character eq] */




/*
	Created: 30 January 2019

	IMPORTANT: First initialize Check_Item_Tables PROCEDURE

	Contains: 
		Main table for all items and subtables for subcategories if needed (e.g. swords have a subtable containing their attributes like damage etc.)
		The completeness of all the data is being checked at the end of the generation by the execution of Check_Item_Tables procedure.
		It is used to specify whether all items in subtables have their general references in the main table.
*/

SET NOCOUNT ON

IF OBJECT_ID('Amulet') IS NOT NULL
	DROP TABLE Amulet

IF OBJECT_ID('Food') IS NOT NULL
	DROP TABLE Food

IF OBJECT_ID('Spell') IS NOT NULL
	DROP TABLE Spell

IF OBJECT_ID('Potion') IS NOT NULL
	DROP TABLE Potion

IF OBJECT_ID('Armor') IS NOT NULL
	DROP TABLE Armor

IF OBJECT_ID('Shield') IS NOT NULL
	DROP TABLE Shield

IF OBJECT_ID('Wand') IS NOT NULL
	DROP TABLE Wand

IF OBJECT_ID('Sword') IS NOT NULL
	DROP TABLE Sword

IF OBJECT_ID('Loot_Trigger') IS NOT NULL
	DROP TRIGGER Loot_Trigger

IF OBJECT_ID('Loot') IS NOT NULL
	DROP TABLE Loot

IF OBJECT_ID('Loot_Boxes') IS NOT NULL
	DROP TABLE Loot_Boxes

IF OBJECT_ID('Items') IS NOT NULL
	DROP TABLE Items

IF OBJECT_ID('Categories') IS NOT NULL
	DROP TABLE Categories

IF OBJECT_ID('Tiers') IS NOT NULL
	DROP TABLE Tiers

CREATE TABLE Categories(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL
)

INSERT INTO Categories([Name]) VALUES
('Currency'),
('Loot Box'),
('Sword'),
('Wand'),
('Shield'),
('Armor'),
('Potion'),
('Spell'),
('Food'),
('Amulet'),
('Miscellaneous')


CREATE TABLE Tiers(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL
)

INSERT INTO Tiers([Name]) VALUES
('Common'),
('Uncommon'),
('Rare'),
('Epic'),
('Legendary')

CREATE TABLE Items(
	ID INT IDENTITY(1, 1) NOT NULL,
	Category VARCHAR(32) NOT NULL,
	Tier VARCHAR(32) NOT NULL,
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	MaxStack INT NOT NULL,

	CONSTRAINT FK_Cat FOREIGN KEY (Category)
    REFERENCES Categories([Name]),

	CONSTRAINT FK_Tier FOREIGN KEY (Tier)
    REFERENCES Tiers([Name])
)

INSERT INTO Items VALUES
	-- Currency
	('Currency', 'Common', 'Gold Coin', 100),

	-- Loot Boxes
	('Loot Box', 'Legendary', 'Undermoon Box', 9999),

	-- Swords
	('Sword', 'Common', 'Steel Sword', 1),
	('Sword', 'Common', 'Bronze Sword', 1),
	('Sword', 'Common', 'Bone Sword', 1),
	('Sword', 'Common', 'Broadsword', 1),
	('Sword', 'Common', 'Dagger', 1),
	('Sword', 'Uncommon', 'Blacksteel Sword', 1),
	('Sword', 'Uncommon', 'Poison Dagger', 1),
	('Sword', 'Rare', 'Crystal Sword', 1),
	('Sword', 'Rare', 'Ice Rapier', 1),
	('Sword', 'Epic', 'Fire Sword', 1),
	('Sword', 'Epic', 'Magic Longsword', 1),
	('Sword', 'Legendary', 'Excalibur', 1),

	-- Wands
	('Wand', 'Rare', 'Dream Blossom Staff', 1),
	('Wand', 'Epic', 'Wand of Darkness', 1),
	('Wand', 'Epic', 'Wand of Inferno', 1),
	('Wand', 'Legendary', 'Wand of Starstorm', 1),

	-- Shields
	('Shield', 'Common', 'Wooden Shield', 1),
	('Shield', 'Uncommon', 'Battle Shield', 1),
	('Shield', 'Rare', 'Ancient Shield', 1),
	('Shield', 'Epic', 'Blessed Shield', 1),
	('Shield', 'Legendary', 'Dragon Shield', 1),

	-- Armors
	('Armor', 'Common', 'Coat', 1),
	('Armor', 'Uncommon', 'Chain Armor', 1),
	('Armor', 'Rare', 'Dark Armor', 1),
	('Armor', 'Epic', 'Crown Armor', 1),
	('Armor', 'Legendary', 'Dragon Robe', 1),
	
	-- Potions
	('Potion', 'Common', 'Water', 1),
	('Potion', 'Uncommon', 'Antidote', 1),
	('Potion', 'Uncommon', 'Poison', 1),
	('Potion', 'Rare', 'Mana Potion', 1),
	('Potion', 'Rare', 'Health Potion', 1),
	('Potion', 'Epic', 'Darkforest Water', 1),
	('Potion', 'Legendary', 'Dragonbreath Potion', 1),

	-- Spells
	('Spell', 'Uncommon', 'Fireball', 100),
	('Spell', 'Uncommon', 'Curse', 100),
	('Spell', 'Rare', 'Ice Strike', 100),
	('Spell', 'Epic', 'Fire Wave', 100),
	('Spell', 'Epic', 'Eternal Winter', 100),
	('Spell', 'Legendary', 'Divine Missle', 100),
	('Spell', 'Legendary', 'Ultimate Flame Strike', 100),

	-- Food
	('Food', 'Common', 'Bread', 100),
	('Food', 'Common', 'Candy', 100),
	('Food', 'Common', 'Meat', 100),
	('Food', 'Uncommon', 'Grapes', 100),
	('Food', 'Rare', 'Fire Mushroom', 100),
	('Food', 'Epic', 'Forbidden Fruit', 100),

	-- Amulets
	('Amulet', 'Uncommon', 'Bronze Amulet', 1),
	('Amulet', 'Rare', 'Crystal Necklace', 1),
	('Amulet', 'Rare', 'Dragon Necklace', 1),
	('Amulet', 'Epic', 'Ancient Amulet', 1),
	('Amulet', 'Epic', 'Amulet of Loss', 1),
	('Amulet', 'Legendary', 'Elven Amulet', 1),

	-- Miscellaneous
	('Miscellaneous', 'Common', 'Dice', 1),
	('Miscellaneous', 'Common', 'Hammer', 1),
	('Miscellaneous', 'Common', 'Leather', 10),
	('Miscellaneous', 'Common', 'Empty Flask', 1),
	('Miscellaneous', 'Common', 'Key', 1),
	('Miscellaneous', 'Common', 'Steel', 10),
	('Miscellaneous', 'Common', 'Wood', 10),
	('Miscellaneous', 'Uncommon', 'Silver Goblet', 1),
	('Miscellaneous', 'Rare', 'Crystal', 100),
	('Miscellaneous', 'Rare', 'Black Pearl', 100),
	('Miscellaneous', 'Epic', 'Phoenix Ash', 100),
	('Miscellaneous', 'Epic', 'Diamond', 100),
	('Miscellaneous', 'Legendary', 'White Pearl', 100)


-- SUBTABLES FOR ITEMS
CREATE TABLE Amulet(
	[Name] VARCHAR(32) PRIMARY KEY,
	Strength INT NOT NULL,
	Dexternity INT NOT NULL,
	Constitution INT NOT NULL,
	Intelligence INT NOT NULL,
	Wisdom INT NOT NULL,
	Charisma INT NOT NULL,

	Health INT NOT NULL,
	Mana INT NOT NULL,
)

INSERT INTO Amulet VALUES
('Bronze Amulet', 0, 0, 0, 0, 0, 5, 50, 0),
('Crystal Necklace', 10, 5, 5, 0, 0, 15, 80, 100),
('Dragon Necklace', 15, 15, 0, 0, 5, 15, 200, 50),
('Ancient Amulet', 20, 15, 5, 5, 10, 20, 0, 300),
('Amulet of Loss', 0, 0, 0, 0, 0, 0, 0, 0), -- makes you not lose items when you die
('Elven Amulet', 30, 30, 30, 30, 30, 30, 300, 1000)


CREATE TABLE Food(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	HP INT NOT NULL,
	Mana INT NOT NULL
)

INSERT INTO Food VALUES
('Bread', 10, 0),
('Candy', 7, 0),
('Meat', 18, 0),
('Grapes', 4, 0),
('Fire Mushroom', 120, 60),
('Forbidden Fruit', 1000, 1000)



CREATE TABLE Spell(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	Damage INT NOT NULL,
	CriticalDamage INT NOT NULL,
	[Range] INT NOT NULL,
	Mana INT NOT NULL
)

INSERT INTO Spell VALUES
('Fireball', 30, 50, 8, 10),
('Curse', 25, 35, 15, 13),
('Ice Strike', 10, 20, 25, 8),
('Fire Wave', 35, 60, 17, 50),
('Eternal Winter', 70, 100, 20, 90),
('Divine Missle', 150, 200, 30, 250),
('Ultimate Flame Strike', 350, 450, 35, 550)

CREATE TABLE Armor(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	Defence INT NOT NULL
)

INSERT INTO Armor VALUES
('Coat', 1),
('Chain Armor', 10),
('Dark Armor', 15),
('Crown Armor', 30),
('Dragon Robe', 50)


CREATE TABLE Shield(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	Defence INT NOT NULL
)

INSERT INTO Shield VALUES
('Wooden Shield', 5),
('Battle Shield', 8),
('Ancient Shield', 13),
('Blessed Shield', 25),
('Dragon Shield', 40)


CREATE TABLE Wand(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	Damage INT NOT NULL,
	CriticalDamage INT NOT NULL,
	[Range] INT NOT NULL,
	Mana INT NOT NULL
)

INSERT INTO Wand VALUES
('Dream Blossom Staff', 70, 100, 10, 30),
('Wand of Darkness', 130, 150, 12, 40),
('Wand of Inferno', 145, 170, 13, 60),
('Wand of Starstorm', 180, 220, 15, 130)

CREATE TABLE Sword(
	[Name] VARCHAR(32) PRIMARY KEY NOT NULL,
	Damage INT NOT NULL,
	CriticalDamage INT NOT NULL,
	MagicDamage INT NOT NULL
)

INSERT INTO Sword VALUES
('Steel Sword', 10, 15, 0),
('Bronze Sword', 7, 10, 0),
('Bone Sword', 2, 5, 1),
('Broadsword', 15, 20, 0),
('Dagger', 5, 10, 0),
('Blacksteel Sword', 20, 25, 0),
('Poison Dagger', 30, 45, 0),
('Crystal Sword', 40, 65, 10),
('Ice Rapier', 30, 70, 10),
('Fire Sword', 60, 100, 30),
('Magic longsword', 65, 130, 50),
('Excalibur', 100, 200, 75)

CREATE TABLE Loot(
	Loot_Box_Name VARCHAR(32) NOT NULL,
	Item_Name VARCHAR(32) NOT NULL,
	Chance INT NOT NULL -- 0 TO 100

	PRIMARY KEY(Loot_Box_Name, Item_Name)
)

GO
CREATE TRIGGER Loot_Trigger ON Loot
INSTEAD OF INSERT
AS
BEGIN TRY
	DECLARE @LootBoxName VARCHAR(32)
	DECLARE @Item_Name VARCHAR(32)
	DECLARE @Chance INT

	DECLARE Cur CURSOR FOR SELECT * FROM inserted
	OPEN Cur
	
	FETCH Cur INTO @LootBoxName, @Item_Name, @Chance

	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF EXISTS(SELECT * FROM Items WHERE [Name] = @LootBoxName)
		BEGIN
			IF EXISTS(SELECT * FROM Items WHERE [Name] = @Item_Name) AND @Chance BETWEEN 0 AND 100
			BEGIN
				INSERT INTO Loot VALUES
				(@LootBoxName, @Item_Name, @Chance)
			END ELSE
			BEGIN
				RAISERROR('Cannot add item %s to LootBox %s', 16, 1, @Item_Name, @LootBoxName)
			END
		END ELSE
		BEGIN
			RAISERROR('%s is not a loot box or does not exist', 16, 1, @LootBoxName)
		END
		FETCH Cur INTO @LootBoxName, @Item_Name, @Chance
	END


	CLOSE Cur
	DEALLOCATE Cur
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(4000)
	DECLARE @ErrorSeverity INT
	DECLARE @ErrorState INT
	SET @ErrorMessage = ERROR_MESSAGE()
	SET @ErrorSeverity = ERROR_SEVERITY()
	SET @ErrorState = ERROR_STATE()

	EXEC ErrorDisplayer @ErrorMessage, @ErrorSeverity, @ErrorState
END CATCH
GO

INSERT INTO Loot VALUES
('Undermoon Box', 'Gold Coin', 50),
('Undermoon Box', 'Wand of Inferno', 70)

--SELECT * FROM Loot


-- FINAL DATA CHECK
DECLARE @Status BIT
EXEC Check_Item_Tables @Return_Status = @Status OUTPUT -- procedure that checks data completeness 



--you need to run character tables first

If OBJECT_ID('LevelTr') is not null
	drop trigger LevelTr
go

create trigger LevelTr on Characters
After update
as	
		if(update(xp))
		begin
			declare i cursor
			for select CharacterID, xp from characters
			for update of [level], XP
			declare @characterID int, @XP int

			open i
			fetch i into  @characterID, @XP
		
			while @@FETCH_STATUS<>-1
			begin 
			
				while(@XP >=1000)
				begin
					exec LevelUp @characterID
					set @XP = @XP - 1000
					update characters set Xp = @XP, [Level] = [Level] + 1 where current of i
				end
				fetch i into  @characterID, @XP
			end
			close i
			deallocate i
		end

go


IF object_id ('Num_acc_dates') is not null -- funkcja zwraca konta, kt�re zosta�y stworzone w okre�lonym przedziale czasu
drop function Num_acc_dateto
go
create function Num_acc_dateto (@datefrom Date, @dateto Date)
returns table
as
return (Select * from registration_logs where CreationDate >= @datefrom and CreationDate <= @dateto)
go

IF object_id ('acc_login_datefrom') is not null -- funkcja zwraca konta, na kt�re gracz logowa� si� od podanej daty
drop function acc_login_datefrom
go
create function acc_login_datefrom (@datefrom Date)
returns table
as
return (Select distinct R.* from registration_logs as R join login_logs as L on R.PlayerID = L.PlayerID where L.DateLogIn >= @datefrom)
go

if OBJECT_ID('get_player_from_characterID') is not null
	drop function get_player_from_characterID
go
create function get_player_from_characterID (@characterID int)
returns int
as
	begin
	declare @id int
	if( exists( select playerID from characters where CharacterId = @characterID))
		select @id = playerID from characters where CharacterId = @characterID
	else
		set @id=0
	return (@id)
	end
go

if OBJECT_ID('ViewAllCharactersInPoland') is not null
	drop view ViewAllCharactersInPoland
go
	create view ViewAllCharactersInPoland as
		select chars.CharacterName from characters as chars where chars.PlayerID = 
		(select PlayerID from registration_logs as reg where reg.Country = 'Poland')
go

Insert into premiums values (0.5,0) , (0,0.5) , (0.25,0.25) -- dodajemy 3 rodzaje konta premium
Exec Log_In 'rootroot','rootroot' -- logujemy roota
